use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fmt;
use std::fmt::Formatter;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() -> Result<(), Box<dyn Error>> {
    let map = parse_file();
    parse_adj_list(&map);

    Ok(())
}

fn parse_adj_list(map: &Map) {
    let start = Point { x: 1, y: 0 };
    let end = Point {
        x: map[0].len() - 2,
        y: map.len() - 1,
    };
    // To work with the existing neighbours function from part one
    let empty = HashSet::new();
    // Our Adjacency list.
    let mut adj = HashMap::new();
    adj.insert(start, Vec::new());
    adj.insert(end, Vec::new());
    let mut nodes = HashSet::new();
    nodes.insert(start);
    nodes.insert(end);

    // Make an adjacency list of junctions, start, and end points
    for i in 1..(map.len() - 1) {
        for j in 1..(map[0].len() - 1) {
            if map[i][j] == Space::Forest {
                continue;
            }
            if neighbours(&map, &Point { x: j, y: i }, &empty).count() > 2 {
                // Is a node for our adjacency list
                adj.insert(Point { x: j, y: i }, Vec::new());
                nodes.insert(Point { x: j, y: i });
            }
        }
    }

    // Find adjacent nodes (and the distance to it) for each
    for node in &nodes {
        let mut path = HashSet::new();
        path.insert(*node);
        if let Some(neighbour_nodes) = search(&map, path, *node, &nodes, &node) {
            adj.entry(*node).and_modify(|el| el.extend(neighbour_nodes));
        }
    }

    let mut path = HashSet::new();
    path.insert((start, 0));
    if let Some(dist) = search_nodes(&adj, path, start, &end) {
        println!("Part Two: {dist}");
    }
}

fn search(
    map: &Map,
    path: HashSet<Point>,
    pos: Point,
    nodes: &HashSet<Point>,
    start: &Point,
) -> Option<Vec<(Point, usize)>> {
    if &pos != start && nodes.contains(&pos) {
        return Some(vec![(pos, path.len())]);
    }
    let neighbours: Vec<Point> = neighbours(&map, &pos, &path).collect();
    if neighbours.len() == 0 {
        return None;
    }

    Some(
        neighbours
            .iter()
            .filter_map(|&neighbour| {
                let mut next_path = path.clone();
                next_path.insert(pos);
                search(&map, next_path, neighbour, &nodes, &start)
            })
            .flatten()
            .collect(),
    )
}

fn search_nodes(
    adj: &HashMap<Point, Vec<(Point, usize)>>,
    path: HashSet<(Point, usize)>,
    pos: Point,
    end: &Point,
) -> Option<usize> {
    if &pos == end {
        return Some(path.iter().map(|(_p, dist)| dist).sum());
    }

    if let Some(neighbours) = adj.get(&pos) {
        if let Some(dist) = neighbours
            .iter()
            .filter(|(neighbour, _)| !path.iter().any(|(el, _)| el == neighbour))
            .map(|(neighbour, dist)| {
                let mut next_path = path.clone();
                next_path.insert((*neighbour, *dist));
                if let Some(dist) = search_nodes(&adj, next_path, *neighbour, &end) {
                    return dist;
                } else {
                    return 0;
                }
            })
            .max()
        {
            return Some(dist);
        }
    }
    None
}

fn neighbours<'a>(
    map: &'a Map,
    p: &'a Point,
    path: &'a HashSet<Point>,
) -> impl Iterator<Item = Point> + 'a {
    // 0, 1, 2 to allow using usize
    [[0, 1], [2, 1], [1, 0], [1, 2]]
        .iter()
        .filter(|[y, _x]| (p.y != 0 || *y != 0) && (p.y != map.len() - 1 || *y != 2))
        .map(move |[y, x]| Point {
            x: p.x - 1 + x,
            y: p.y + y - 1,
        })
        .filter(|new_p| map[new_p.y][new_p.x] != Space::Forest)
        .filter(|new_p| !path.contains(new_p))
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Space {
    Path,
    Forest,
}

impl Space {
    fn new(value: char) -> Self {
        match value {
            '#' => Self::Forest,
            '.' => Self::Path,
            '^' => Self::Path,
            '>' => Self::Path,
            'v' => Self::Path,
            '<' => Self::Path,
            _ => panic!("Unknown space {}", value),
        }
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
struct Point {
    x: usize,
    y: usize,
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "[{}, {}]", self.x, self.y)
    }
}
impl fmt::Debug for Point {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "[{}, {}]", self.x, self.y)
    }
}

type Map = [[Space; 141]; 141];

fn parse_file() -> Map {
    let file = File::open("input/day23.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut map = [[Space::Forest; 141]; 141];

    lines.map(|l| l.unwrap()).enumerate().for_each(|(i, l)| {
        l.chars().enumerate().for_each(|(j, ch)| {
            map[i][j] = Space::new(ch);
        })
    });
    map
}
