use std::collections::HashSet;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() -> Result<(), Box<dyn Error>> {
    let (start, map) = parse_file();

    // let result = part_one(&map, start, 64);
    let result = part_two(&map, start, 26501365);

    println!("Part Two: {}", result);
    Ok(())
}

fn part_two(map: &Map, start: Point, req_steps: usize) -> usize {
    // Map tile size
    let cell_size = map.len();
    // Distance from start to edge of map tile (there are no rocks straight to any edge)
    let dist_to_edge = cell_size / 2;
    // Steps
    let y = [
        // First tile (starting at halfway)
        part_one(&map, start, dist_to_edge),
        // Next full tile
        part_one(&map, start, dist_to_edge + cell_size),
        // Next full tile (don't need any more than this to get to the required steps)
        part_one(&map, start, dist_to_edge + 2 * cell_size),
    ];
    // Quadratic formula
    let a = (y[2] - (2 * y[1]) + y[0]) / 2;
    let b = y[1] - y[0] - a;
    let c = y[0];
    let n = (req_steps - dist_to_edge) / cell_size;
    a * n.pow(2) + (b * n) + c
}

fn part_one(map: &Map, start: Point, req_steps: usize) -> usize {
    let mut positions: HashSet<Point> = HashSet::new();
    positions.insert(start);
    let mut removes: Vec<Point> = Vec::new();
    let mut adds: Vec<Point> = Vec::new();

    for _ in 0..req_steps {
        for p in &positions {
            let neighbours = neighbours(&map, &p);
            // Branch and step to each neighbour
            for n in neighbours {
                adds.push(n);
            }
            removes.push(*p);
        }
        // Remove after adding all the neighbours so that we're not mutating the vector while iterating
        while let Some(p) = removes.pop() {
            positions.remove(&p);
        }
        // Add to positions after removes are done, in case a neighbour moved into a spot that
        // was previously occupied by a position
        while let Some(p) = adds.pop() {
            positions.insert(p);
        }
    }

    positions.len()
}

fn is_rock(map: &Map, [i, j]: &Point) -> bool {
    let mut y;
    let mut x;

    if *i < 0 {
        y = map.len() - (usize::try_from(i.abs()).unwrap() % map.len());
        if y == map.len() {
            y = 0;
        }
    } else if *i >= isize::try_from(map.len() - 1).unwrap() {
        y = usize::try_from(*i).unwrap() % map.len();
    } else {
        y = (*i).try_into().unwrap();
    }
    if *j < 0 {
        x = map.len() - (usize::try_from(j.abs()).unwrap() % map.len());
        if x == map[0].len() {
            x = 0;
        }
    } else if *j >= isize::try_from(map.len() - 1).unwrap() {
        x = usize::try_from(*j).unwrap() % map.len();
    } else {
        x = (*j).try_into().unwrap();
    }

    map[y][x] == Space::Rock
}

fn neighbours<'a>(map: &'a Map, [i, j]: &'a Point) -> impl Iterator<Item = Point> + 'a {
    // Using 0, 1, 2 to allow using usize points
    [[-1, 0], [1, 0], [0, -1], [0, 1]]
        .iter()
        .map(move |[y, x]| [i + y, j + x] as Point)
        .filter(|p| !is_rock(map, p))
}

#[derive(Debug, PartialEq, Eq, Copy, Clone)]
enum Space {
    Garden,
    Rock,
    Start,
}

impl Space {
    fn new(value: char) -> Self {
        match value {
            '.' => Self::Garden,
            '#' => Self::Rock,
            'S' => Self::Start,
            _ => panic!("Unknown space type. {}", value),
        }
    }
}

type Map = [[Space; 131]; 131];
type Point = [isize; 2];
fn parse_file() -> (Point, Map) {
    let file = File::open("input/day21.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut map: Map = [[Space::Garden; 131]; 131];
    let mut start: Point = [0, 0];

    lines.map(|l| l.unwrap()).enumerate().for_each(|(i, l)| {
        l.chars().enumerate().for_each(|(j, ch)| {
            map[i][j] = Space::new(ch);
            if ch == 'S' {
                start = [i.try_into().unwrap(), j.try_into().unwrap()];
            }
        })
    });
    (start, map)
}
