use std::cmp::Ordering;
use std::collections::BinaryHeap;
use std::collections::HashSet;
use std::error::Error;
use std::fs::File;
use std::hash::Hash;
use std::hash::Hasher;
use std::io::BufRead;
use std::io::BufReader;

/// Run Dijkstra's algorithm to find the lowest cost path.
fn main() -> Result<(), Box<dyn Error>> {
    let city = parse_file();
    let target_pos = (city.len() - 1, city[0].len() - 1);
    let mut visited: HashSet<Step> = HashSet::new();
    let mut to_visit: BinaryHeap<Step> = BinaryHeap::new();
    let min_steps = 4;
    let max_steps = 10;
    // Two starting states. Either start going right or going down.
    [Direction::Right, Direction::Down]
        .into_iter()
        .for_each(|dir| {
            to_visit.push(Step {
                x: 0,
                y: 0,
                dist: 0,
                dir,
                dir_steps: 0,
            });
        });

    // Visit steps in the priority queue
    while let Some(point) = to_visit.pop() {
        // Stop at target as long as we've moved the minimum number of steps to get there
        if point.y == target_pos.0 && point.x == target_pos.1 && point.dir_steps >= min_steps {
            println!("Part Two: {:?}", point.dist);
            break;
        }
        // Find valid neighbours
        for neighbour in neighbours(&city, &point, min_steps, max_steps) {
            if visited.insert(neighbour) {
                // This neighbour has not been visited yet
                to_visit.push(neighbour);
            }
        }
    }

    Ok(())
}

/// Return neighbours that are valid based on the constraints
fn neighbours<'a>(
    grid: &'a City,
    point: &'a Step,
    min_steps: usize,
    max_steps: usize,
) -> impl Iterator<Item = Step> + 'a {
    // Try moving in each direction
    Direction::iter()
        .filter(move |&dir| {
            // Can't go backwards
            dir != point.dir.opposite()
                // Can't go more than max steps
                && (point.dir_steps < max_steps || point.dir != dir)
                // Must go at least min steps in the same direction
                && (point.dir_steps >= min_steps || point.dir == dir)
        })
        .filter_map(|dir| {
            // move_in_dir will only return Some(point) that are inside the bounds
            return if let Some(point) =
                move_in_dir(grid.len() - 1, grid.len() - 1, (point.y, point.x), dir)
            {
                Some((dir, point))
            } else {
                // Point was out of bounds
                None
            };
        })
        .map(|(dir, (y, x))| Step {
            x,
            y,
            dir,
            // Calculate the distance at the current point to this neighbour
            dist: point.dist + grid[y][x],
            dir_steps: if dir == point.dir {
                // Continue in the same direction
                point.dir_steps + 1
            } else {
                // First step in this direction
                1
            },
        })
}

/// Return Some(point) heading in the given direction and None if the point would be out of bounds.
fn move_in_dir(
    max_x: usize,
    max_y: usize,
    pos: (usize, usize),
    dir: Direction,
) -> Option<(usize, usize)> {
    match dir {
        Direction::Up => {
            if pos.0 == 0 {
                None
            } else {
                Some((pos.0 - 1, pos.1))
            }
        }
        Direction::Down => {
            if pos.0 == max_y {
                None
            } else {
                Some((pos.0 + 1, pos.1))
            }
        }
        Direction::Left => {
            if pos.1 == 0 {
                None
            } else {
                Some((pos.0, pos.1 - 1))
            }
        }
        Direction::Right => {
            if pos.1 == max_x {
                None
            } else {
                Some((pos.0, pos.1 + 1))
            }
        }
    }
}

#[derive(Debug, Clone, Copy)]
struct Step {
    x: usize,
    y: usize,
    dist: usize,
    dir: Direction,
    dir_steps: usize,
}

impl Hash for Step {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        self.y.hash(state);
        // Dir and steps are considered unique to, because going through the same point
        // could have different direction or steps in that direction.
        self.dir_steps.hash(state);
        self.dir.hash(state);
    }
}

impl PartialEq<Self> for Step {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x
            && self.y == other.y
            && self.dir_steps == other.dir_steps
            && self.dir == other.dir
    }
}

impl PartialOrd<Self> for Step {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

impl Eq for Step {}

impl Ord for Step {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        // Shorter distance should be considered greater
        other.dist.cmp(&self.dist)
    }
}

#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn iter() -> impl Iterator<Item = Self> {
        [
            Direction::Up,
            Direction::Left,
            Direction::Down,
            Direction::Right,
        ]
        .into_iter()
    }

    fn opposite(&self) -> Self {
        match self {
            Direction::Up => Direction::Down,
            Direction::Right => Direction::Left,
            Direction::Down => Direction::Up,
            Direction::Left => Direction::Right,
        }
    }
}

type City = [[usize; 141]; 141];

fn parse_file() -> City {
    let file = File::open("input/day17.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut city: City = [[0; 141]; 141];

    lines.map(|l| l.unwrap()).enumerate().for_each(|(i, l)| {
        l.chars()
            .enumerate()
            .for_each(|(j, ch)| city[i][j] = ch.to_string().parse::<usize>().unwrap())
    });
    city
}
