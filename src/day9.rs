use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() -> Result<(), Box<dyn Error>> {
    let sequences = parse_file();
    let total: isize = sequences.iter().map(|seq| predict(&seq)).sum();
    println!("Part Two: {total}");
    Ok(())
}

fn predict(seq: &Vec<isize>) -> isize {
    let dif_seq: Vec<isize> = seq.windows(2).map(|w| w[1] - w[0]).collect();
    let prediction = if dif_seq.iter().all(|&el| el == 0) {
        *seq.first().unwrap()
    } else {
        *seq.first().unwrap() - predict(&dif_seq)
    };
    prediction
}

fn parse_file() -> Vec<Vec<isize>> {
    let file = File::open("input/day9.txt").unwrap();
    let lines = BufReader::new(file).lines();
    lines
        .map(|l| l.unwrap())
        .map(|l| {
            l.split(" ")
                .map(|el| el.parse::<isize>().unwrap())
                .collect::<Vec<isize>>()
        })
        .collect::<Vec<Vec<isize>>>()
}
