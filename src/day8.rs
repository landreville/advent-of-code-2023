use num::Integer;
use regex::Regex;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::iter::Cycle;
use std::str::Chars;

fn main() -> Result<(), Box<dyn Error>> {
    let (dir_str, adj) = parse_file();
    let directions = dir_str.chars().cycle();

    let steps = adj
        .keys()
        .filter(|&k| k.chars().last() == Some('A'))
        .map(|node| follow_path(&adj, node, directions.clone()))
        .reduce(|acc, e| e.lcm(&acc))
        .unwrap();

    println!("Part Two: {steps}");
    Ok(())
}

fn follow_path(
    adj: &HashMap<String, [String; 2]>,
    node: &String,
    mut directions: Cycle<Chars>,
) -> usize {
    let mut choice = directions.next();
    let mut next_node = node;
    let mut steps = 0;
    while next_node.chars().last() != Some('Z') {
        next_node = match choice {
            Some('L') => &adj.get(next_node).unwrap()[0],
            Some('R') => &adj.get(next_node).unwrap()[1],
            _ => panic!("Invalid direction"),
        };
        choice = directions.next();
        steps += 1;
    }
    steps
}

fn parse_file() -> (String, HashMap<String, [String; 2]>) {
    let file = File::open("input/day8.txt").unwrap();
    let mut lines = BufReader::new(file).lines();
    let mut adj: HashMap<String, [String; 2]> = HashMap::new();
    let re = Regex::new(r"(?<node>[A-Z0-9]{3}) = \((?<left>[A-Z0-9]{3}), (?<right>[A-Z0-9]{3})\)")
        .expect("Invalid regex");
    let directions = lines.next().unwrap().unwrap();

    lines.next();
    for line in lines {
        if let Ok(line) = line {
            if let Some(caps) = re.captures(&line) {
                adj.insert(
                    caps["node"].to_string(),
                    [caps["left"].to_string(), caps["right"].to_string()],
                )
            } else {
                println!("No match while parsing.");
                continue;
            };
        }
    }

    (directions, adj)
}
