use regex::Regex;
use std::convert::TryInto;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() -> Result<(), Box<dyn Error>> {
    let digs = parse_file();
    let mut path: Vec<Point> = vec![Point { x: 0, y: 0 }];

    for dig in digs {
        let prev = path.last().unwrap().clone();
        let points = prev.move_point(dig.dir, dig.dist);
        path.extend(points);
    }

    // Shoelace algorithm
    let area = (0..path.len())
        .map(|i| {
            if i == 0 {
                path[i].x * (path[i + 1].y - path[path.len() - 1].y)
            } else if i == path.len() - 1 {
                path[i].x * (path[0].y - path[i - 1].y)
            } else {
                path[i].x * (path[i + 1].y - path[i - 1].y)
            }
        })
        .sum::<isize>()
        / 2;

    // Pick's theorem for the interior points
    let b: isize = (path.len() - 1).try_into().unwrap();
    let i = area + 1 - b / 2;

    let total = i + b;

    println!("Part Two: {total}");

    Ok(())
}

fn print_path(path: &Vec<Point>) {
    let max_x = path.iter().map(|p| p.x).max().unwrap();
    let max_y = path.iter().map(|p| p.y).max().unwrap();
    let min_x = path.iter().map(|p| p.x).min().unwrap();
    let min_y = path.iter().map(|p| p.y).min().unwrap();
    for i in min_y..=max_y {
        'x_loop: for j in min_x..=max_x {
            for p in path {
                if p.y == i && p.x == j {
                    print!("#");
                    continue 'x_loop;
                }
            }
            print!(".");
        }
        print!("\n");
    }
    print!("\n");
}

#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
struct Point {
    x: isize,
    y: isize,
}

impl Point {
    fn move_point(&self, dir: Direction, dist: isize) -> impl Iterator<Item = Self> + '_ {
        (0..dist).map(move |i| match dir {
            Direction::Up => Point {
                x: self.x,
                y: self.y - (1 + i),
            },
            Direction::Down => Point {
                x: self.x,
                y: self.y + (1 + i),
            },
            Direction::Left => Point {
                x: self.x - (1 + i),
                y: self.y,
            },
            Direction::Right => Point {
                x: self.x + (1 + i),
                y: self.y,
            },
        })
    }
}

#[derive(Debug, PartialEq, Clone, Hash, Eq)]
struct Dig {
    dir: Direction,
    dist: isize,
}

impl Dig {
    fn new(value: &str) -> Self {
        let dist = isize::from_str_radix(&value[..5], 16).unwrap();
        Dig {
            dir: Direction::new(&value[5..6]),
            dist,
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

impl Direction {
    fn new(value: &str) -> Self {
        match value {
            "3" => Direction::Up,
            "2" => Direction::Left,
            "0" => Direction::Right,
            "1" => Direction::Down,
            _ => panic!("Unknown dir value {}", value),
        }
    }
}

fn parse_file() -> Vec<Dig> {
    let file = File::open("input/day18.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let re =
        Regex::new(r"(?<direction>L|R|U|D) (?<distance>[0-9]+) \(#(?<colour>[0-9a-zA-Z]{6})\)")
            .unwrap();

    lines
        .map(|l| l.unwrap())
        .map(|l| {
            if let Some(caps) = re.captures(&l) {
                Dig::new(&caps["colour"])
            } else {
                panic!("Could not parse line {}", l);
            }
        })
        .collect::<Vec<Dig>>()
}
