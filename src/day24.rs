use nalgebra::{Matrix3, Matrix6, Vector3, Vector6};
use regex::Regex;
use std::error::Error;
use std::fmt;
use std::fmt::Formatter;
use std::fs::File;
use std::hash::{Hash, Hasher};
use std::io::BufRead;
use std::io::BufReader;
use std::ops::RangeInclusive;

fn main() -> Result<(), Box<dyn Error>> {
    let stones = parse_file();
    // let test_area = 200000000000000f64..=400000000000000f64;
    // let mut total = 0;
    //
    // for i in 0..stones.len() {
    //     for j in (i + 1)..stones.len() {
    //         let a_stone = stones[i];
    //         let b_stone = stones[j];
    //
    //         if let Some(_) = a_stone.intersects(&b_stone, &test_area) {
    //             total += 1;
    //         }
    //     }
    // }
    //
    // println!("Part One: {total}");

    part_two(&stones);

    Ok(())
}

fn part_two(stones: &Vec<Hail>) {
    /*
    Algebra Time!
    P -> start position of rock
    t -> time step
    V -> velocity of rock
    p[n] -> start position of hail n
    v[n] -> velocity of hail n

    P + t * V = p[n] + t * v[n]
    P + t * V - P[n] = t * v[n]
    P - P[n] = t * v[n] - t * V
    P - P[n] = v[n] - V
    Cross-Product both sides with (V - v[n]) and the right-hand side cancels out to zero
    (P - P[n]) x (V - v[n]) = (v[n] - V) x (V - v[n])
    (P - P[n]) x (V - v[n]) = 0

     */
    let a = stones[0];
    let b = stones[1];
    let c = stones[2];

    // -p[0] x v[0] + p[1] x v[1]
    let first = -a.point().cross(&a.velocity()) + b.point().cross(&b.velocity());
    // -p[0] x v[0] + p[2] x v[2]
    let second = -a.point().cross(&a.velocity()) + c.point().cross(&c.velocity());

    let rhs: Vector6<f64> = Vector6::new(first.x, first.y, first.z, second.x, second.y, second.z);

    let tl = cross_matrix(a.velocity()) - cross_matrix(b.velocity());
    let bl = cross_matrix(a.velocity()) - cross_matrix(c.velocity());
    let tr = -cross_matrix(a.point()) + cross_matrix(b.point());
    let br = -cross_matrix(a.point()) + cross_matrix(c.point());

    let mut m: Matrix6<f64> = Matrix6::new(
        tl.m11, tl.m12, tl.m13, tr.m11, tr.m12, tr.m13, // Row 1
        tl.m21, tl.m22, tl.m23, tr.m21, tr.m22, tr.m23, // Row 2
        tl.m31, tl.m32, tl.m33, tr.m31, tr.m32, tr.m33, // Row 3
        bl.m11, bl.m12, bl.m13, br.m11, br.m12, br.m13, // Row 4
        bl.m21, bl.m22, bl.m23, br.m21, br.m22, br.m23, // Row 5
        bl.m31, bl.m32, bl.m33, br.m31, br.m32, br.m33, // Row 6
    );

    if let Some(m) = m.try_inverse() {
        let result = m * rhs;
        println!("{:?}", result);

        let total: isize = (0..3).map(|i| result[i] as isize).sum();
        println!("Part Two: {total}");
    }
}

fn cross_matrix(v: Vector3<f64>) -> Matrix3<f64> {
    Matrix3::new(
        0.0, -v[2], v[1], // Row 1
        v[2], 0.0, -v[0], // Row 2
        -v[1], v[0], 0.0, // Row 3
    )
}

#[derive(Clone, Copy)]
struct Hail {
    x: isize,
    y: isize,
    z: isize,
    vx: f64,
    vy: f64,
    vz: f64,
}

impl Hail {
    fn point(&self) -> Vector3<f64> {
        Vector3::new(self.x as f64, self.y as f64, self.z as f64)
    }

    fn velocity(&self) -> Vector3<f64> {
        Vector3::new(self.vx, self.vy, self.vz)
    }

    fn slope(&self) -> f64 {
        if self.vx == 0f64 {
            f64::INFINITY
        } else {
            self.vy as f64 / self.vx as f64
        }
    }

    fn intersects(&self, other: &Self, bounds: &RangeInclusive<f64>) -> Option<[f64; 2]> {
        let x: f64;
        let y: f64;
        if self.slope() == other.slope() {
            return None;
        } else if self.slope() == f64::INFINITY {
            x = self.x as f64;
            y = other.slope() * (x - other.x as f64) + other.y as f64;
        } else if other.slope() == f64::INFINITY {
            x = other.x as f64;
            y = self.slope() * (x - self.x as f64) + self.y as f64;
        } else {
            x = (self.y as f64 - other.y as f64 - self.x as f64 * self.slope()
                + other.x as f64 * other.slope())
                / (other.slope() - self.slope());
            y = self.y as f64 + self.slope() * (x - self.x as f64);
        }

        if bounds.contains(&x)
            && bounds.contains(&y)
            && (((x - self.x as f64) >= 0.0 && self.vx >= 0.0)
                || ((x - self.x as f64) < 0.0 && self.vx < 0.0))
            && (((x - other.x as f64) >= 0.0 && other.vx >= 0.0)
                || ((x - other.x as f64) < 0.0 && other.vx < 0.0))
        {
            Some([x, y])
        } else {
            None
        }
    }
}

impl fmt::Display for Hail {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}, {}, {} @ {}, {}, {}",
            self.x, self.y, self.z, self.vx, self.vy, self.vz
        )
    }
}

impl fmt::Debug for Hail {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self)
    }
}

impl Hash for Hail {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.x.hash(state);
        self.y.hash(state);
        self.z.hash(state);
    }
}

impl PartialEq<Self> for Hail {
    fn eq(&self, other: &Self) -> bool {
        self.x == other.x && self.y == other.y && self.z == other.z
    }
}

impl Eq for Hail {}

fn parse_file() -> Vec<Hail> {
    let file = File::open("input/day24.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut stones = Vec::new();
    let re = Regex::new(
        r"(?<x1>\d+), (?<y1>\d+), (?<z1>\d+) @ *(?<vx>-?\d+), *(?<vy>-?\d+), *(?<vz>-?\d+)",
    )
    .unwrap();

    lines.map(|l| l.unwrap()).for_each(|l| {
        let Some(caps) = re.captures(&l) else {
            panic!("Invalid hail. {}", l);
        };

        stones.push(Hail {
            x: caps["x1"].parse::<isize>().unwrap(),
            y: caps["y1"].parse::<isize>().unwrap(),
            z: caps["z1"].parse::<isize>().unwrap(),
            vx: caps["vx"].parse::<f64>().unwrap(),
            vy: caps["vy"].parse::<f64>().unwrap(),
            vz: caps["vz"].parse::<f64>().unwrap(),
        });
    });

    stones
}
