use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::collections::HashMap;

fn main() -> Result<(), Box<dyn Error>> {
    let steps = parse_file();
    let total: usize = steps.iter().map(|ords| hash(ords)).sum();
    println!("Part One: {total}");
    part_two(&steps);
    Ok(())
}

fn hash(ords: &[u32]) -> usize {
    usize::try_from(ords.iter().fold(0, |acc, e| ((acc + e) * 17) % 256)).unwrap()
}

fn part_two(steps: &Vec<Vec<u32>>) {
    let dash = '-' as u32;
    let equal = '=' as u32;
    let mut boxes: HashMap<usize, Vec<(&[u32], usize)>> = HashMap::new();

    for step in steps {
        let op_pos = step.iter().position(|&el| el == dash || el == equal).unwrap();
        let label = &step[..op_pos];
        let box_number = hash(label);

        let op = if step[op_pos] == dash {
            Op::Dash
        } else {
            Op::Equal(char::from_u32(step[op_pos+1]).unwrap().to_string().parse::<usize>().unwrap())
        };

        match op {
            Op::Dash => {
                let entry = boxes.entry(box_number).or_insert(Vec::new());
                if let Some(pos) = entry.iter().position(|(lbl, _fl)| *lbl == label) {
                    entry.remove(pos);
                }
            },
            Op::Equal(focal_length) => {
                let entry = boxes.entry(box_number).or_insert(Vec::new());
                if let Some(pos) = entry.iter().position(|(lbl, _fl)| *lbl == label) {
                    entry.remove(pos);
                    entry.insert(pos, (label, focal_length));
                } else {
                    entry.push((label, focal_length));
                }
            }
        }
    }

    let total: usize = boxes.iter().filter(|(|_box_number, lenses)| lenses.len() > 0)
        .map(|(box_number, lenses)| {
            lenses.iter().enumerate().map(|(i, (_label, fl))| (box_number + 1) * (i + 1) * fl).sum::<usize>()
        })
        .sum();

    println!("Part Two: {total}");
}
#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum Op {
    Dash,
    Equal(usize)
}

fn parse_file() -> Vec<Vec<u32>> {
    let file = File::open("input/day15.txt").unwrap();
    let mut lines = BufReader::new(file).lines();

    if let Some(Ok(line)) = lines.next() {
        return line.split(",")
            .map(|s| s.chars().map(|ch| {
                ch as u32
            }).collect())
            .collect();
    }
    panic!("No lines found");
}