use std::cmp::max;
use std::error::Error;
use std::fs;
use pest::iterators::Pair;
use pest::Parser;
use pest_derive::Parser;

#[derive(Parser)]
#[grammar_inline = "\
colour = { \"red\" | \"green\" | \"blue\" }
digit = { '0'..'9' }
game_number = { digit+ }
colour_count = { digit+ }
game = { \"Game\" ~ \" \" ~ game_number ~ \": \" ~ set ~ (\"; \" ~ set)* }
pull = { colour_count+ ~ \" \" ~ colour }
set = { pull ~ (\", \" ~ pull)*}
file = { SOI ~ game ~ (NEWLINE ~ game )* ~ EOI }
"]
pub struct GameParser;

fn main() -> Result<(), Box<dyn Error>>{
    let file = fs::read_to_string("input/day2.txt").expect("Cannot read file");

    let parsed = GameParser::parse(Rule::file, &file)
        .expect("Parse error")
        .next().unwrap();

    let mut games = [[0;3];100];

    for game in parsed.into_inner() {
        match game.as_rule() {
            Rule::game => { parse_game(&mut games, game) },
            Rule::EOI => (),
            _ => panic!("unknown rule {:?}", game)
        }
    }

    let test = [12,13,14];

    let total: usize = games
        .iter()
        .enumerate()
        .filter(|(_i, game_set)| game_set.iter().enumerate().all(|(j, v)| test[j] >= *v))
        .map(|(i, _game_set)| i+1)
        .sum();

    println!("Part One: {total}");

    let p2: usize = games.iter()
        .map(|game_set| game_set[0] * game_set[1] * game_set[2])
        .sum();

    println!("Part Two: {p2}");

    return Ok(())
}

fn parse_game(games: &mut [[usize; 3]; 100], game_pair: Pair<Rule>) {
    let mut game_number = 0;

    for game_pair in game_pair.into_inner() {
        match game_pair.as_rule() {
            Rule::game_number => game_number = game_pair.as_str().parse::<usize>()
                .expect("Game number invalid"),
            Rule::set =>  {
                let colour_counts = parse_set(game_pair);
                for i in 0..3 {
                    games[game_number-1][i] = max(games[game_number-1][i], colour_counts[i]);
                }
            }
            _ => panic!("Unknown game rule {:?}", game_pair)
        }
    }
}

fn parse_set(set_pair: Pair<Rule>) -> [usize; 3] {
    let mut colour_counts = [0;3];

    for pull_pair in set_pair.into_inner() {
        let (colour_count, colour_index) = parse_pull(pull_pair);
        colour_counts[colour_index] = colour_count;
    }

    colour_counts
}

fn parse_pull(pull_pair: Pair<Rule>) -> (usize, usize) {
    let mut colour_count = None;
    let mut colour_index = None;

    for pair in pull_pair.into_inner() {
        match pair.as_rule() {
            Rule::colour_count => colour_count = Some(pair.as_str().parse::<usize>()
                .expect("Invalid colour count")),
            Rule::colour => colour_index = Some(index_colour(&pair.as_str())),
            _ => panic!("Unknown colour rule {:?}", pair)
        }
    }

    if colour_count.is_none() || colour_index.is_none() {
        panic!("Invalid colour pull")
    }

    (colour_count.unwrap(), colour_index.unwrap())
}

fn index_colour(colour: &str) -> usize {
    match colour {
        "red" => 0,
        "green" => 1,
        "blue" => 2,
        _ => panic!("Invalid colour {}", colour)
    }
}