mod day2;
mod day3;

use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;
use std::cmp::min;

fn main() {
    partone();
    parttwo();
}

fn parttwo() {
    let digits: [&str; 18] = [
        "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
        "1", "2", "3", "4", "5", "6", "7", "8", "9"
    ];

    let file = File::open("input/day1.txt").unwrap();
    let lines = BufReader::new(file).lines();

    let total: usize = lines.map(|line| find_first_and_last(&digits, line.unwrap())).sum();
    println!("Part Two: {total}")
}

fn find_first_and_last(digits: &[&str], line: String) -> usize {
    let mut first: Option<usize> = None;
    let mut last: Option<usize> = None;
    let line_len = line.chars().count();

    for i in 0..line_len {
        for (j, &digit) in digits.iter().enumerate() {
            if first.is_some() && last.is_some() {
                break;
            }
            let digit_len = digit.chars().count();

            if first.is_none() {
                let check_first = &line[i..min(i+digit_len, line_len)];
                if check_first == digit {
                    first = Some(if (j+1)%9 == 0 {9} else {(j+1) % 9});
                }
            }

            if last.is_none() && line_len >= digit_len + i{
                let check_last = &line[line_len - i - digit_len..line_len - i];
                if check_last == digit {
                    last = Some(if (j+1)%9 == 0 {9} else {(j+1) % 9});
                }
            }
        }
    }


    let first_digit = first.unwrap();
    let last_digit = last.unwrap();

    format!("{first_digit}{last_digit}").parse::<usize>().unwrap()
}

fn partone() {
    let file = File::open("input/day1.txt").unwrap();
    let lines = BufReader::new(file).lines();

    let total: usize = lines.map(|line| combine_first_and_last_digit(line.unwrap())).sum();
    println!("Part One: {total}")
}

fn combine_first_and_last_digit(line: String) -> usize {
    let chars: Vec<char> = line.chars().collect();
    let mut first: Option<String> = None;
    let mut last: Option<String> = None;

    for i in 0..chars.len() {
        if first.is_some() && last.is_some() {
            break;
        }

        if first.is_none() && chars[i].to_string().parse::<usize>().is_ok(){
            first = Some(chars[i].to_string());
        }
        if last.is_none() && chars[chars.len()-i-1].to_string().parse::<usize>().is_ok(){
            last = Some(chars[chars.len()-i-1].to_string());
        }
    }

    let first_digit = first.unwrap();
    let last_digit = last.unwrap();
    format!("{first_digit}{last_digit}").parse::<usize>().unwrap()
}
