use crate::Rule::dest_range_start;
use itertools::Itertools;
use pest::iterators::Pair;
use pest::Parser;
use pest_derive::Parser;
use rayon::prelude::*;
use std::cmp::Ordering;
use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fs;
use std::ops::Range;

#[derive(Parser)]
#[grammar_inline = "\
WHITESPACE = _{ \" \" | NEWLINE }
digit = { '0'..'9' }
item = { \"seed\" | \"soil\" | \"fertilizer\" | \"water\" | \"light\" | \"temperature\" | \"humidity\" | \"location\"}
dest_range_start = @{ digit+ }
source_range_start = @{ digit+ }
length = @{ digit+ } 
seed = @{digit+ }
source_item = @{ item }
dest_item = @{ item }

seeds = { \"seeds:\" ~ seed+ }
mapping = { dest_range_start ~ source_range_start ~ length }
map = { source_item ~ \"-to-\" ~ dest_item ~ \"map:\" ~ mapping+ }
file = { SOI ~ seeds ~ map+ ~ EOI }
"]
pub struct MapParser;
#[derive(Eq, Hash, PartialEq, Debug, Copy, Clone)]
enum Item {
    Seed,
    Soil,
    Fertilizer,
    Water,
    Light,
    Temperature,
    Humidity,
    Location,
}
#[derive(Eq, PartialEq)]
struct Mapping {
    dest_range_start: usize,
    source_range_start: usize,
    length: usize,
}

impl Ord for Mapping {
    fn cmp(&self, other: &Self) -> Ordering {
        self.dest_range_start.cmp(&other.dest_range_start)
    }
}

impl PartialOrd for Mapping {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

struct ItemMap {
    source_item: Item,
    dest_item: Item,
    mappings: Vec<Mapping>,
}

impl ItemMap {
    fn translate(&self, source_value: usize) -> usize {
        let mut mapped_value = source_value;
        for mapping in &self.mappings {
            if source_value >= mapping.source_range_start
                && source_value < mapping.source_range_start + mapping.length
            {
                mapped_value = mapping.dest_range_start + source_value - mapping.source_range_start;
            }
        }
        mapped_value
    }

    fn reverse_translate(&self, dest_value: usize) -> usize {
        let mut mapped_value: usize = dest_value;

        for mapping in &self.mappings {
            if dest_value >= mapping.dest_range_start
                && dest_value < mapping.dest_range_start + mapping.length
            {
                mapped_value = mapping.source_range_start + dest_value - mapping.dest_range_start;
            }
        }
        mapped_value
    }
}
fn main() -> Result<(), Box<dyn Error>> {
    let file = fs::read_to_string("input/day5.txt").expect("Cannot read file");

    let parsed = MapParser::parse(Rule::file, &file)
        .expect("Parse error")
        .next()
        .unwrap();

    let mut seeds: Vec<usize> = Vec::new();
    let mut maps: HashMap<Item, ItemMap> = HashMap::new();

    for pair in parsed.into_inner() {
        match pair.as_rule() {
            Rule::seeds => seeds = parse_seeds(pair),
            Rule::map => {
                let item_map = parse_map(pair);
                maps.insert(item_map.source_item, item_map);
            }
            Rule::EOI => (),
            _ => panic!("unknown rule {:?}", pair),
        }
    }

    part_two(&seeds, &maps);

    Ok(())
}

fn part_one(seeds: &Vec<usize>, maps: &HashMap<Item, ItemMap>) {
    let mut lowest_location = 0;
    let mut pot_loc = 0;
    for seed in seeds {
        pot_loc = translate_item(maps, Item::Seed, *seed);
        if lowest_location == 0 || pot_loc < lowest_location {
            lowest_location = pot_loc;
        }
    }

    println!("Part One: {lowest_location}");
}

fn part_two(seeds: &Vec<usize>, maps: &HashMap<Item, ItemMap>) {
    let mut reverse_maps: HashMap<Item, &ItemMap> = HashMap::new();
    maps.values().for_each(|m| {
        reverse_maps.insert(m.dest_item, m);
    });
    let loc_map = reverse_maps.get(&Item::Location).unwrap();
    let mut seed = 0;
    let mut seed_ranges: Vec<Range<usize>> = Vec::new();
    let mut seed_start = 0;
    let mut seed_length = 0;
    let mut found = false;

    for seed_chunk in seeds.chunks(2) {
        seed_start = seed_chunk[0];
        seed_length = seed_chunk[1];
        seed_ranges.push(seed_start..(seed_start + seed_length));
    }

    'toplevel: for mapping in &loc_map.mappings {
        for i in mapping.dest_range_start..(mapping.dest_range_start + mapping.length) {
            seed = reverse_translate_item(&reverse_maps, Item::Location, i);
            found = false;
            'seedranges: for seed_range in &seed_ranges {
                if seed_range.contains(&seed) {
                    found = true;
                    println!("{i}");
                    break 'toplevel;
                }
            }
        }
    }

    println!("Part Two: {location}");
}

fn reverse_translate_item(
    reverse_maps: &HashMap<Item, &ItemMap>,
    dest_item: Item,
    dest_value: usize,
) -> usize {
    if dest_item == Item::Seed {
        return dest_value;
    }
    if let Some(item_map) = reverse_maps.get(&dest_item) {
        return reverse_translate_item(
            reverse_maps,
            item_map.source_item,
            item_map.reverse_translate(dest_value),
        );
    }
    unreachable!("Unknown item type {:?}", dest_item);
}

fn translate_item(maps: &HashMap<Item, ItemMap>, source_item: Item, source_value: usize) -> usize {
    if source_item == Item::Location {
        return source_value;
    }

    if let Some(item_map) = maps.get(&source_item) {
        return translate_item(maps, item_map.dest_item, item_map.translate(source_value));
    }

    unreachable!("Unknown item type {:?}", source_item);
}

fn parse_seeds(pair: Pair<Rule>) -> Vec<usize> {
    pair.into_inner()
        .map(|el| match el.as_rule() {
            Rule::seed => el.as_str().parse::<usize>().unwrap(),
            _ => panic!("unknown rule {:?}", el),
        })
        .collect::<Vec<usize>>()
}

fn parse_map(pair: Pair<Rule>) -> ItemMap {
    let mut item_map = ItemMap {
        source_item: Item::Seed,
        dest_item: Item::Seed,
        mappings: Vec::new(),
    };
    for map_pair in pair.into_inner() {
        match map_pair.as_rule() {
            Rule::dest_item => item_map.dest_item = str_to_item(map_pair.as_str()),
            Rule::source_item => item_map.source_item = str_to_item(map_pair.as_str()),
            Rule::mapping => item_map.mappings.push(parse_mapping(map_pair)),
            _ => panic!("Unknown rule {:?}", map_pair),
        }
    }
    item_map.mappings.sort();
    item_map
}

fn parse_mapping(pair: Pair<Rule>) -> Mapping {
    let mut mapping = Mapping {
        source_range_start: 0,
        dest_range_start: 0,
        length: 0,
    };
    for sub_pair in pair.into_inner() {
        match sub_pair.as_rule() {
            Rule::dest_range_start => {
                mapping.dest_range_start = sub_pair.as_str().parse::<usize>().unwrap()
            }
            Rule::source_range_start => {
                mapping.source_range_start = sub_pair.as_str().parse::<usize>().unwrap()
            }
            Rule::length => mapping.length = sub_pair.as_str().parse::<usize>().unwrap(),
            _ => panic!("Unknown rule {:?}", sub_pair),
        }
    }
    mapping
}

fn str_to_item(value: &str) -> Item {
    match value {
        "seed" => Item::Seed,
        "soil" => Item::Soil,
        "fertilizer" => Item::Fertilizer,
        "water" => Item::Water,
        "light" => Item::Light,
        "temperature" => Item::Temperature,
        "humidity" => Item::Humidity,
        "location" => Item::Location,
        _ => unreachable!("Invalid item value {}", value),
    }
}
