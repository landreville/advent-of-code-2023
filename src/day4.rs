use pest::iterators::Pair;
use pest::Parser;
use pest_derive::Parser;
use std::collections::HashSet;
use std::error::Error;
use std::fs;
use std::ops::Range;

#[derive(Parser)]
#[grammar_inline = "\
WHITESPACE = _{ \" \" }
digit = { '0'..'9' }
winning_number = @{ digit+ }
card_number = @{ digit+ }
game_number = @{ digit+ }
game = { \"Card \" ~ game_number ~ \": \" ~ winning_number+ ~ \"|\" ~ card_number+ }
file = { SOI ~ game ~ (NEWLINE ~ game )* ~ EOI }
"]
pub struct LottoParser;

#[derive(Debug, Clone)]
struct LottoCard {
    game_number: usize,
    winning_numbers: HashSet<usize>,
    card_numbers: HashSet<usize>,
}

impl LottoCard {
    fn score(&self) -> usize {
        let count = self
            .card_numbers
            .intersection(&self.winning_numbers)
            .count();
        let mut score = 0;
        if count == 1 {
            score = 1
        } else if count > 1 {
            score = usize::pow(2, (count - 1).try_into().unwrap())
        }
        score
    }

    fn win_cards(&self) -> Range<usize> {
        self.game_number + 1
            ..self.game_number
                + 1
                + self
                    .card_numbers
                    .intersection(&self.winning_numbers)
                    .count()
    }
}

fn main() -> Result<(), Box<dyn Error>> {
    let file = fs::read_to_string("input/day4.txt").expect("Cannot read file");

    let parsed = LottoParser::parse(Rule::file, &file)
        .expect("Parse error")
        .next()
        .unwrap();

    let mut cards: Vec<LottoCard> = Vec::new();

    for pair in parsed.into_inner() {
        match pair.as_rule() {
            Rule::game => cards.push(parse_game(pair)),
            Rule::EOI => (),
            _ => panic!("unknown rule {:?}", pair),
        }
    }

    let part1: usize = cards.iter().map(|card| card.score()).sum();
    println!("Part One: {}", part1);

    let total = part2(&cards);
    println!("{total}");

    Ok(())
}

fn part2(cards: &Vec<LottoCard>) -> usize {
    let mut cards_to_process: Vec<&LottoCard> = cards.iter().map(|card| card).collect();
    cards_to_process.reverse();

    let mut count = 0;
    while let Some(card) = cards_to_process.pop() {
        count = count + 1;
        for game_number in card.win_cards() {
            cards_to_process.push(&cards[game_number - 1])
        }
    }
    count
}

fn parse_game(pair: Pair<Rule>) -> LottoCard {
    let mut card = LottoCard {
        game_number: 0,
        winning_numbers: HashSet::new(),
        card_numbers: HashSet::new(),
    };

    for card_pair in pair.into_inner() {
        match card_pair.as_rule() {
            Rule::game_number => card.game_number = card_pair.as_str().parse::<usize>().unwrap(),
            Rule::winning_number => {
                card.winning_numbers
                    .insert(card_pair.as_str().parse::<usize>().unwrap());
            }
            Rule::card_number => {
                card.card_numbers
                    .insert(card_pair.as_str().parse::<usize>().unwrap());
            }
            _ => panic!("Unknown rule {:?}", card_pair),
        }
    }

    card
}
