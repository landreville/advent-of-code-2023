use core::iter::zip;
use std::cmp::Ordering;
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::io::BufReader;

fn main() -> Result<(), Box<dyn Error>> {
    let mut hands = parse_file();
    hands.sort();

    let total: usize = hands
        .iter()
        .rev()
        .enumerate()
        .map(|(i, h)| (i + 1) * h.bet)
        .sum();

    println!("Part One: {total}");
    Ok(())
}

#[derive(Copy, Clone, Debug, PartialEq)]
enum Card {
    A,
    K,
    Q,
    T,
    Nine,
    Eight,
    Seven,
    Six,
    Five,
    Four,
    Three,
    Two,
    J,
}
const CARD_ORDER: [Card; 13] = [
    Card::A,
    Card::K,
    Card::Q,
    Card::T,
    Card::Nine,
    Card::Eight,
    Card::Seven,
    Card::Six,
    Card::Five,
    Card::Four,
    Card::Three,
    Card::Two,
    Card::J,
];

#[derive(Debug, PartialEq, Clone, Copy)]
enum HandType {
    FiveKind,
    FourKind,
    FullHouse,
    ThreeKind,
    TwoPair,
    OnePair,
    HighCard,
}

const HAND_ORDER: [HandType; 7] = [
    HandType::FiveKind,
    HandType::FourKind,
    HandType::FullHouse,
    HandType::ThreeKind,
    HandType::TwoPair,
    HandType::OnePair,
    HandType::HighCard,
];

#[derive(Debug)]
struct Hand {
    cards: [Card; 5],
    bet: usize,
    hand_type: HandType,
}

impl PartialEq for Hand {
    fn eq(&self, other: &Self) -> bool {
        self.cards == other.cards
    }
}

impl Eq for Hand {}

impl PartialOrd for Hand {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

impl Ord for Hand {
    fn cmp(&self, other: &Self) -> Ordering {
        if self.hand_type == other.hand_type {
            for (self_card, other_card) in zip(self.cards, other.cards) {
                if self_card == other_card {
                    continue;
                }
                return CARD_ORDER
                    .iter()
                    .position(|&el| el == self_card)
                    .unwrap()
                    .cmp(&CARD_ORDER.iter().position(|&el| el == other_card).unwrap());
            }
        } else {
            return HAND_ORDER
                .iter()
                .position(|&el| el == self.hand_type)
                .unwrap()
                .cmp(
                    &HAND_ORDER
                        .iter()
                        .position(|&el| el == other.hand_type)
                        .unwrap(),
                );
        }
        Ordering::Equal
    }
}

impl Hand {
    fn new(cards: &str, bet: usize) -> Self {
        let hand_cards = Hand::parse_cards(cards);
        Self {
            cards: hand_cards,
            bet,
            hand_type: Hand::parse_hand_type(hand_cards),
        }
    }

    fn parse_cards(cards: &str) -> [Card; 5] {
        let mut hand: [Card; 5] = [Card::A; 5];
        for (i, ch) in cards.chars().enumerate() {
            hand[i] = match ch {
                'A' => Card::A,
                'K' => Card::K,
                'Q' => Card::Q,
                'J' => Card::J,
                'T' => Card::T,
                '9' => Card::Nine,
                '8' => Card::Eight,
                '7' => Card::Seven,
                '6' => Card::Six,
                '5' => Card::Five,
                '4' => Card::Four,
                '3' => Card::Three,
                '2' => Card::Two,
                _ => panic!("Unknown card type {}", ch),
            }
        }
        hand
    }

    fn parse_hand_type(cards: [Card; 5]) -> HandType {
        let mut counts: [usize; 13] = [0; 13];
        for card in cards {
            counts[CARD_ORDER.iter().position(|&el| el == card).unwrap()] += 1;
        }

        let max_i: usize = counts[0..12]
            .iter()
            .enumerate()
            .max_by(|(_, &a), (_, &b)| a.cmp(&b))
            .map(|(i, _)| i)
            .unwrap();
        counts[max_i] += counts[12];
        counts[12] = 0;

        if counts.iter().any(|&v| v >= 5) {
            return HandType::FiveKind;
        }
        if counts.iter().any(|&v| v >= 4) {
            return HandType::FourKind;
        }
        if counts.iter().any(|&v| v == 3) && counts.iter().any(|&v| v == 2) {
            return HandType::FullHouse;
        }
        if counts.iter().any(|&v| v >= 3) {
            return HandType::ThreeKind;
        }
        if counts.iter().filter(|&v| *v == 2).count() >= 2 {
            return HandType::TwoPair;
        }
        if counts.iter().any(|&v| v == 2) {
            return HandType::OnePair;
        }
        return HandType::HighCard;
    }
}
fn parse_file() -> Vec<Hand> {
    let file = File::open("input/day7.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut hands: Vec<Hand> = Vec::new();

    for line in lines {
        if let Ok(line) = line {
            let mut parts = line.split(" ");
            let cards = parts.next().unwrap();
            let bet = parts.next().unwrap().parse::<usize>().unwrap();
            hands.push(Hand::new(cards, bet));
        }
    }

    hands
}
