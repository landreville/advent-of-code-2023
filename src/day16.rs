use std::collections::HashSet;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

type Board = [[Tile; 110]; 110];
const MAX_BOARD: usize = 109;
fn main() -> Result<(), Box<dyn Error>> {
    let board = parse_file();

    let mut starts: Vec<Beam> = Vec::new();

    for (y, dir) in [(0, Direction::Down), (109, Direction::Up)] {
        for x in 0..board[0].len() {
            starts.push(Beam { pos: (y, x), dir });
        }
    }

    for (x, dir) in [(0, Direction::Right), (109, Direction::Left)] {
        for y in 0..board.len() {
            starts.push(Beam { pos: (y, x), dir });
        }
    }

    let total = starts
        .iter()
        .map(|&beam| energize_tiles(&board, beam))
        .max()
        .unwrap();

    println!("Part Two: {total}");

    Ok(())
}

fn energize_tiles(board: &Board, start_beam: Beam) -> usize {
    let mut energized = [[false; 110]; 110];
    let mut duplicates = HashSet::new();
    let mut active_beams = vec![start_beam];
    duplicates.insert(start_beam);
    energized[start_beam.pos.0][start_beam.pos.1] = true;

    while active_beams.len() > 0 {
        let beam = active_beams.pop().unwrap();

        let beams = next_beam(&board, beam);
        beams.iter().filter_map(|&el| el).for_each(|bm| {
            energized[bm.pos.0][bm.pos.1] = true;
            if !duplicates.contains(&bm) {
                duplicates.insert(bm);
                active_beams.push(bm);
            }
        });
    }

    let total: usize = energized
        .iter()
        .map(|row| row.iter().filter(|&el| *el).count())
        .sum::<usize>();
    total
}

fn next_beam(board: &Board, beam: Beam) -> [Option<Beam>; 2] {
    let pos = beam.pos;
    let mut beam_a: Option<Beam> = None;
    let mut beam_b: Option<Beam> = None;

    let tile = board[pos.0][pos.1];
    let [move_dir_a, move_dir_b] = match tile {
        Tile::Empty => [Some(beam.dir), None],
        // /
        Tile::ForwardMirror => [
            Some(match beam.dir {
                Direction::Up => Direction::Right,
                Direction::Down => Direction::Left,
                Direction::Left => Direction::Down,
                Direction::Right => Direction::Up,
            }),
            None,
        ],
        // \
        Tile::BackMirror => [
            Some(match beam.dir {
                Direction::Up => Direction::Left,
                Direction::Down => Direction::Right,
                Direction::Left => Direction::Up,
                Direction::Right => Direction::Down,
            }),
            None,
        ],
        Tile::HSplit => match beam.dir {
            Direction::Up => [Some(Direction::Left), Some(Direction::Right)],
            Direction::Down => [Some(Direction::Left), Some(Direction::Right)],
            Direction::Left => [Some(beam.dir), None],
            Direction::Right => [Some(beam.dir), None],
        },
        Tile::VSplit => match beam.dir {
            Direction::Left => [Some(Direction::Up), Some(Direction::Down)],
            Direction::Right => [Some(Direction::Up), Some(Direction::Down)],
            Direction::Up => [Some(beam.dir), None],
            Direction::Down => [Some(beam.dir), None],
        },
    };

    if let Some(move_dir_a) = move_dir_a {
        if let Some(next_pos) = move_in_dir(beam.pos, move_dir_a) {
            beam_a = Some(Beam {
                pos: next_pos,
                dir: move_dir_a,
            });
        }
    }

    if let Some(move_dir_b) = move_dir_b {
        if let Some(next_pos) = move_in_dir(beam.pos, move_dir_b) {
            beam_b = Some(Beam {
                pos: next_pos,
                dir: move_dir_b,
            });
        }
    }

    [beam_a, beam_b]
}

fn move_in_dir(pos: (usize, usize), dir: Direction) -> Option<(usize, usize)> {
    match dir {
        Direction::Up => {
            if pos.0 == 0 {
                None
            } else {
                Some((pos.0 - 1, pos.1))
            }
        }
        Direction::Down => {
            if pos.0 == MAX_BOARD {
                None
            } else {
                Some((pos.0 + 1, pos.1))
            }
        }
        Direction::Left => {
            if pos.1 == 0 {
                None
            } else {
                Some((pos.0, pos.1 - 1))
            }
        }
        Direction::Right => {
            if pos.1 == MAX_BOARD {
                None
            } else {
                Some((pos.0, pos.1 + 1))
            }
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
struct Beam {
    pos: (usize, usize),
    dir: Direction,
}

#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum Tile {
    Empty,
    ForwardMirror,
    BackMirror,
    VSplit,
    HSplit,
}
fn parse_file() -> Board {
    let file = File::open("input/day16.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut board: Board = [[Tile::Empty; 110]; 110];

    lines.map(|l| l.unwrap()).enumerate().for_each(|(i, l)| {
        l.chars().enumerate().for_each(|(j, ch)| {
            board[i][j] = match ch {
                '.' => Tile::Empty,
                '/' => Tile::ForwardMirror,
                '\\' => Tile::BackMirror,
                '|' => Tile::VSplit,
                '-' => Tile::HSplit,
                _ => panic!("Unknown char {ch}"),
            }
        })
    });

    board
}
