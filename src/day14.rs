use itertools::Itertools;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() -> Result<(), Box<dyn Error>> {
    let mut platform = parse_file();
    // Prepend a row of cubes to act as blocks
    platform.insert(0, vec![Space::Cube; platform[0].len()]);

    let mut weights: Vec<usize> = Vec::new();
    let mut weight: usize = 0;

    for cycle in 0..1000000000 {
        for direction in [Cardinal::N, Cardinal::W, Cardinal::S, Cardinal::E] {
            for x in 0..platform[0].len() {
                for y in 0..platform.len() {
                    match platform[y][x] {
                        Space::Cube => (),
                        Space::Empty => (),
                        Space::Round => {
                            if let Some(block) =
                                (0..=y - 1).rev().find(|&i| platform[i][x] != Space::Empty)
                            {
                                platform[y][x] = Space::Empty;
                                platform[block + 1][x] = Space::Round;
                            }
                        }
                    }
                }
            }
            platform = rotate_platform(&platform);
        }

        weights.push(weigh_platform(&platform));
        if let Some(cycle_start) = detect_cycle(&weights) {
            let cycle_diff = cycle + 1 - cycle_start;
            let cycles_left = 1000000000 - cycle_start;
            let offset = cycles_left % cycle_diff;
            let index = cycle_start + offset - 1;
            weight = weights[index];
            break;
        }
    }

    println!("Part Two: {weight}");
    Ok(())
}

fn detect_cycle(weights: &Vec<usize>) -> Option<usize> {
    for i in 1..weights.len() {
        let test: Vec<usize> = weights.iter().rev().take(i).map(|&v| v).collect();
        let cycle = weights
            .iter()
            .rev()
            .skip(i)
            .chunks(i)
            .into_iter()
            .take(3)
            .all(|ch| ch.map(|&v| v).collect::<Vec<usize>>() == test);
        if cycle {
            return Some(weights.len() - i);
        }
    }
    None
}

fn rotate_platform(platform: &Vec<Vec<Space>>) -> Vec<Vec<Space>> {
    let mut rotated: Vec<Vec<Space>> =
        vec![vec![Space::Empty; platform[0].len()]; platform[0].len()];
    for (i, row) in platform[1..].iter().enumerate() {
        for (j, col) in row.iter().enumerate() {
            rotated[j][platform[0].len() - 1 - i] = *col;
        }
    }
    rotated.insert(0, vec![Space::Cube; platform[0].len()]);
    rotated
}

#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum Cardinal {
    N,
    W,
    S,
    E,
}

fn weigh_platform(platform: &Vec<Vec<Space>>) -> usize {
    (0..platform.len())
        .rev()
        .enumerate()
        .map(|(m, i)| (m + 1) * platform[i].iter().filter(|&el| *el == Space::Round).count())
        .sum()
}

fn print_platform(platform: &Vec<Vec<Space>>) {
    print!("\n");
    for row in platform {
        for col in row {
            print!(
                "{}",
                match col {
                    Space::Empty => ".",
                    Space::Round => "O",
                    Space::Cube => "#",
                }
            );
        }
        print!("\n");
    }
}
#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum Space {
    Round,
    Cube,
    Empty,
}
fn parse_file() -> Vec<Vec<Space>> {
    let file = File::open("input/day14.txt").unwrap();
    let lines = BufReader::new(file).lines();
    lines
        .map(|l| l.unwrap())
        .map(|l| {
            l.chars()
                .map(|ch| match ch {
                    '.' => Space::Empty,
                    '#' => Space::Cube,
                    'O' => Space::Round,
                    _ => panic!("unexpected stone"),
                })
                .collect()
        })
        .collect()
}
