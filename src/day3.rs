use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, Lines};

fn main() -> Result<(), Box<dyn Error>> {
    let file = File::open("input/day3.txt").unwrap();
    let lines = BufReader::new(file).lines();
    solve(lines);
    Ok(())
}
#[derive(Clone, Debug)]
enum Part {
    Empty,
    Symbol(String),
    Value(String),
}

fn solve(lines: Lines<BufReader<File>>) {
    let schematic: Vec<Vec<Part>> = lines
        .map(|line| line.unwrap())
        .map(|line| {
            line.chars()
                .map(|ch| char_to_part(&ch))
                .collect::<Vec<Part>>()
        })
        .collect::<Vec<Vec<Part>>>();

    let mut part_numbers = HashMap::new();
    schematic
        .iter()
        .enumerate()
        .map(|(i, row)| scan_row(&schematic, i, &row))
        .flatten()
        .for_each(|((i, j), part_number)| {
            part_numbers.insert((i, j), part_number);
        });

    let part_one: usize = part_numbers.values().sum();
    println!("Part One: {part_one}");

    let mut gears = HashMap::new();
    for (i, row) in schematic.iter().enumerate() {
        for (j, col) in row.iter().enumerate() {
            match &schematic[i][j] {
                Part::Symbol(val) if val == "*" => {
                    for (k, l) in adjacent_part_values(&schematic, i, j) {
                        gears
                            .entry((i, j))
                            .or_insert(HashSet::new())
                            .insert(scan_part_number(&schematic, k, l));
                    }
                }
                _ => continue,
            }
        }
    }

    let mut part_two = 0;
    for (_gear, starts) in gears {
        if starts.len() == 2 {
            part_two += starts
                .iter()
                .map(|((_, _), part_number)| part_number)
                .product::<usize>();
        }
    }
    println!("Part Two: {part_two}");
}

fn scan_row(schematic: &Vec<Vec<Part>>, i: usize, row: &Vec<Part>) -> Vec<((usize, usize), usize)> {
    let part_numbers_on_row = row
        .iter()
        .enumerate()
        .filter(|(j, _part)| is_adjacent_to_symbol(&schematic, i, *j))
        .map(|(j, _part)| scan_part_number(&schematic, i, j))
        .collect::<Vec<((usize, usize), usize)>>();
    part_numbers_on_row
}

fn is_adjacent_to_symbol(schematic: &Vec<Vec<Part>>, i: usize, j: usize) -> bool {
    (match schematic[i][j] {
        Part::Value(_) => true,
        _ => false,
    } && ((i > 0 && is_symbol(&schematic[i - 1][j]))
        || (i < 139 && is_symbol(&schematic[i + 1][j]))
        || (j > 0 && is_symbol(&schematic[i][j - 1]))
        || (j < 139 && is_symbol(&schematic[i][j + 1]))
        || (i > 0 && j > 0 && is_symbol(&schematic[i - 1][j - 1]))
        || (i > 0 && j < 139 && is_symbol(&schematic[i - 1][j + 1]))
        || (i < 139 && j > 0 && is_symbol(&schematic[i + 1][j - 1]))
        || (i < 139 && j < 139 && is_symbol(&schematic[i + 1][j + 1]))))
}

fn adjacent_part_values(schematic: &Vec<Vec<Part>>, i: usize, j: usize) -> Vec<(usize, usize)> {
    let mut val_parts: Vec<(usize, usize)> = Vec::new();
    if i > 0 && is_value(&schematic[i - 1][j]) {
        val_parts.push((i - 1, j))
    }
    if i < 139 && is_value(&schematic[i + 1][j]) {
        val_parts.push((i + 1, j));
    }
    if j > 0 && is_value(&schematic[i][j - 1]) {
        val_parts.push((i, j - 1));
    }
    if j < 139 && is_value(&schematic[i][j + 1]) {
        val_parts.push((i, j + 1));
    }
    if i > 0 && j > 0 && is_value(&schematic[i - 1][j - 1]) {
        val_parts.push((i - 1, j - 1));
    }
    if i > 0 && j < 139 && is_value(&schematic[i - 1][j + 1]) {
        val_parts.push((i - 1, j + 1));
    }
    if i < 139 && j > 0 && is_value(&schematic[i + 1][j - 1]) {
        val_parts.push((i + 1, j - 1));
    }
    if i < 139 && j < 139 && is_value(&schematic[i + 1][j + 1]) {
        val_parts.push((i + 1, j + 1));
    }
    val_parts
}

fn is_symbol(part: &Part) -> bool {
    match part {
        Part::Symbol(_) => true,
        _ => false,
    }
}

fn is_value(part: &Part) -> bool {
    match part {
        Part::Value(_) => true,
        _ => false,
    }
}

fn scan_part_number(schematic: &Vec<Vec<Part>>, i: usize, j: usize) -> ((usize, usize), usize) {
    let mut start = j;

    while start > 0
        && match schematic[i][start - 1] {
            Part::Value(_) => true,
            _ => false,
        }
    {
        start = start - 1;
    }

    let part_number = schematic[i][start..140]
        .iter()
        .take_while(|part| match part {
            Part::Value(_) => true,
            _ => false,
        })
        .map(|part| match part {
            Part::Value(val) => val.clone(),
            _ => unreachable!(),
        })
        .collect::<Vec<String>>()
        .join("")
        .parse::<usize>()
        .unwrap();

    ((i, start), part_number)
}

fn char_to_part(ch: &char) -> Part {
    match ch {
        '.' => Part::Empty,
        ch if ch.is_digit(10) => Part::Value(ch.to_string()),
        _ => Part::Symbol(ch.to_string()),
    }
}
