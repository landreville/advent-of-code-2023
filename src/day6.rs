use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let times: [usize; 4] = [45, 98, 83, 73];
    let distances: [usize; 4] = [295, 1734, 1278, 1210];

    let partone: usize = (0..4)
        .map(|i| ways_to_win(times[i], distances[i]))
        .product();
    println!("Part One: {partone}");

    part_two();

    Ok(())
}

fn part_two() {
    let time: usize = 45988373;
    let distance: usize = 295173412781210;

    let mut current_t: usize = time / 2;
    let mut lower_bound: usize = 0;
    let mut upper_bound: usize = 0;

    // Binary search rough bounds
    while time_wins(distance, time, current_t) {
        lower_bound = current_t;
        current_t = current_t / 2;

        if current_t <= 1 {
            break;
        }
    }

    current_t = time / 2;
    while time_wins(distance, time, current_t) {
        upper_bound = current_t;
        current_t = current_t + (time - current_t) / 2;

        if current_t >= time {
            break;
        }
    }

    // Fine tune boundaries
    current_t = lower_bound;
    while time_wins(distance, time, current_t) {
        lower_bound = current_t;
        current_t = current_t - 1;

        if current_t <= 0 {
            break;
        }
    }

    current_t = upper_bound;
    while time_wins(distance, time, current_t) {
        upper_bound = current_t;
        current_t = current_t + 1;

        if current_t >= time {
            break;
        }
    }

    let ways = upper_bound - lower_bound + 1;
    println!("Part Two: {ways}");
}

fn time_wins(distance: usize, time: usize, current_t: usize) -> bool {
    (time - current_t) * current_t > distance
}

fn ways_to_win(time: usize, distance: usize) -> usize {
    let mut ways_to_win = 0;
    for i in 1..time {
        if (time - i) * i > distance {
            ways_to_win += 1;
        }
    }
    ways_to_win
}
