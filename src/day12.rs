use cached::proc_macro::cached;
use rayon::prelude::*;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() -> Result<(), Box<dyn Error>> {
    let (spring_report, range_report) = parse_file();

    let total: usize = spring_report
        .into_par_iter()
        .zip(range_report)
        .map(|(springs, ranges)| count_possible_arangements(springs.clone(), ranges))
        .sum();

    println!("Part Two: {total}");

    Ok(())
}

fn count_possible_arangements(mut springs: Vec<Spring>, counts: Vec<usize>) -> usize {
    // to make the Damaged recursion case simpler
    springs.push(Spring::Operational);
    let mut cache = vec![vec![None; springs.len()]; counts.len()];
    count_possible_arangements_inner(&springs, &counts, &mut cache)
}

fn count_possible_arangements_inner(
    springs: &[Spring],
    counts: &[usize],
    cache: &mut [Vec<Option<usize>>],
) -> usize {
    if counts.is_empty() {
        return if springs.contains(&Spring::Damaged) {
            // Too many previous unknowns were counted as damaged
            0
        } else {
            // All remaining unknowns are operational
            1
        };
    }
    if springs.len() < counts.iter().sum::<usize>() + counts.len() {
        // Not enough space for remaining numbers
        return 0;
    }
    if let Some(cached) = cache[counts.len() - 1][springs.len() - 1] {
        return cached;
    }
    let mut arangements = 0;
    if springs[0] != Spring::Damaged {
        // Assume operational
        arangements += count_possible_arangements_inner(&springs[1..], counts, cache);
    }
    let next_group_size = counts[0];
    if !springs[..next_group_size].contains(&Spring::Operational)
        && springs[next_group_size] != Spring::Damaged
    {
        // Assume damaged
        arangements +=
            count_possible_arangements_inner(&springs[next_group_size + 1..], &counts[1..], cache);
    }
    cache[counts.len() - 1][springs.len() - 1] = Some(arangements);
    arangements
}

fn combos_with_cache(springs: &[Spring], ranges: &[usize]) -> usize {
    let mut cache = vec![vec![None; springs.len()]; ranges.len()];
    find_combos_by_recursion(&mut cache, springs, ranges)
}

fn find_combos_by_recursion(
    cache: &mut [Vec<Option<usize>>],
    springs: &[Spring],
    ranges: &[usize],
) -> usize {
    if ranges.is_empty() {
        return 1;
    }

    if springs.len() < ranges.iter().sum::<usize>() + ranges.len() - 1 {
        // Not enough springs left for the ranges required.
        return 0;
    }

    if let Some(cached) = cache[ranges.len() - 1][springs.len() - 1] {
        return cached;
    }

    let mut combos = 0;

    let next_count = ranges[0];

    if !springs[..next_count].contains(&Spring::Operational) {
        if next_count >= springs.len() {
            combos += 1;
        } else if springs[next_count] != Spring::Damaged {
            // Consume damaged/unknown count
            combos += find_combos_by_recursion(cache, &springs[next_count + 1..], &ranges[1..]);
        }
    }

    if springs[0] == Spring::Operational || springs[0] == Spring::Unknown {
        // Consider operational if unknown or already operational
        combos += find_combos_by_recursion(cache, &springs[1..], &ranges);
    }

    cache[ranges.len() - 1][springs.len() - 1] = Some(combos);

    return combos;
}

#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum Spring {
    Damaged,
    Operational,
    Unknown,
}
fn parse_file() -> (Vec<Vec<Spring>>, Vec<Vec<usize>>) {
    let file = File::open("input/day12.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut springs: Vec<Vec<Spring>> = Vec::new();
    let mut ranges: Vec<Vec<usize>> = Vec::new();

    for line in lines {
        if let Ok(line) = line {
            let (spring_row, counts) = line.split_once(' ').unwrap();
            let spring_row: Vec<Spring> = spring_row
                .chars()
                .map(|c| match c {
                    '.' => Spring::Operational,
                    '#' => Spring::Damaged,
                    '?' => Spring::Unknown,
                    _ => panic!("Unexpected spring."),
                })
                .collect();
            let counts: Vec<usize> = counts
                .split(',')
                .map(|s| s.parse::<usize>().unwrap())
                .collect();
            springs.push(
                spring_row
                    .iter()
                    .copied()
                    .chain([Spring::Unknown])
                    .cycle()
                    .take(spring_row.len() * 5 + 4)
                    .collect(),
            );
            ranges.push(
                counts
                    .iter()
                    .copied()
                    .cycle()
                    .take(counts.len() * 5)
                    .collect(),
            );
        }
    }

    (springs, ranges)
}
