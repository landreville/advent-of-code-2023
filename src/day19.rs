use pest::iterators::Pair;
use pest::Parser;
use pest_derive::Parser;
use std::cmp::{max, min};
use std::collections::HashMap;
use std::error::Error;
use std::fs;
use std::ops::Range;

fn main() -> Result<(), Box<dyn Error>> {
    let file = fs::read_to_string("input/day19.txt").expect("Cannot read file");
    let parsed = WorkflowParser::parse(Rule::file, &file)
        .expect("Parse error")
        .next()
        .unwrap();

    let mut parts: Vec<Part> = Vec::new();
    let mut workflows: HashMap<String, Workflow> = HashMap::new();

    for pair in parsed.into_inner() {
        match pair.as_rule() {
            Rule::workflow => {
                let wf = parse_workflow(pair);
                workflows.insert(wf.name.clone(), wf);
            }
            Rule::part => parts.push(parse_part(pair)),
            Rule::EOI => (),
            _ => (),
        }
    }

    part_one(&parts, &workflows);

    part_two(&workflows);

    Ok(())
}

fn part_two(workflows: &HashMap<String, Workflow>) {
    // let mut over = [4000; 4];
    // let mut under = [0; 4];
    // let cats = [Category::X, Category::M, Category::A, Category::S];
    // let start = workflows.get("in").unwrap();
    let start_dest = Destination::Workflow("in".to_string());

    // let accept_ranges: Vec<[Range<usize>; 4]> = vec![];
    let init_range: [Range<usize>; 4] = [1..4001, 1..4001, 1..4001, 1..4001];

    let total = limit_range(&workflows, start_dest, init_range);

    println!("Part Two: {total}");
}

fn limit_range(
    workflows: &HashMap<String, Workflow>,
    next_dest: Destination,
    mut rng: [Range<usize>; 4],
) -> usize {
    match next_dest {
        Destination::Accept => return rng.iter().map(|r| r.end - r.start).product(),
        Destination::Reject => return 0,
        _ => (),
    }

    let mut total: usize = 0;

    if let Destination::Workflow(next_dest) = next_dest {
        let wf = workflows.get(&next_dest).unwrap();
        for rule in &wf.rules {
            let i = usize::from(rule.cat);

            match (rng[i].clone(), rule.op) {
                (r, WorkflowOp::LessThan) if r.end <= rule.check => {
                    return total + limit_range(workflows, rule.dest.clone(), rng);
                }
                (r, WorkflowOp::GreaterThan) if r.start > rule.check => {
                    return total + limit_range(workflows, rule.dest.clone(), rng);
                }
                (r, WorkflowOp::LessThan) if r.start < rule.check => {
                    rng[i] = r.start..rule.check;
                    total += limit_range(workflows, rule.dest.clone(), rng.clone());
                    rng[i] = rule.check..r.end;
                }
                (r, WorkflowOp::GreaterThan) if r.end >= rule.check => {
                    rng[i] = (rule.check + 1)..r.end;
                    total += limit_range(workflows, rule.dest.clone(), rng.clone());
                    rng[i] = r.start..(rule.check + 1);
                }
                (_, WorkflowOp::LessThan) | (_, WorkflowOp::GreaterThan) => todo!(),
            }
        }

        if let Some(dest) = &wf.default {
            return total + limit_range(workflows, dest.clone(), rng);
        }
    }
    total
}

fn part_one(parts: &Vec<Part>, workflows: &HashMap<String, Workflow>) {
    let mut next_dest: Destination = Destination::Workflow("in".to_string());
    let mut total: usize = 0;
    for part in parts {
        while let Destination::Workflow(dest) = next_dest {
            if let Some(wf) = workflows.get(&dest) {
                next_dest = wf.apply(&part);
            } else {
                panic!("Could not find workflow {:?}", dest);
            }
        }

        match next_dest {
            Destination::Accept => total += part.rating(),
            Destination::Reject => (),
            Destination::Workflow(_) => panic!("Should not have left while loop"),
        }
        next_dest = Destination::Workflow("in".to_string());
    }

    println!("Part One: {total}");
}

fn parse_workflow(pair: Pair<Rule>) -> Workflow {
    let mut rules: Vec<WorkflowRule> = Vec::new();
    let mut name: Option<String> = None;
    let mut default_dest: Option<Destination> = None;

    for pair in pair.into_inner() {
        match pair.as_rule() {
            Rule::workflow_name => name = Some(pair.as_str().to_string()),
            Rule::check => rules.push(parse_rule(pair)),
            Rule::default_dest => default_dest = Some(Destination::parse(pair.as_str())),
            _ => (),
        }
    }

    if let Some(name) = name {
        return Workflow {
            name,
            rules,
            default: default_dest,
        };
    }
    panic!("Could not parse workflow.");
}

fn parse_rule(pair: Pair<Rule>) -> WorkflowRule {
    let mut cat: Option<Category> = None;
    let mut op: Option<WorkflowOp> = None;
    let mut check: Option<usize> = None;
    let mut dest: Option<Destination> = None;

    for pair in pair.into_inner() {
        match pair.as_rule() {
            Rule::category => cat = Some(Category::parse(pair.as_str())),
            Rule::comp => op = Some(WorkflowOp::parse(pair.as_str())),
            Rule::number => check = Some(pair.as_str().parse::<usize>().unwrap()),
            Rule::dest => dest = Some(Destination::parse(pair.as_str())),
            _ => (),
        }
    }

    if let (Some(cat), Some(op), Some(check), Some(dest)) = (cat, op, check, dest) {
        return WorkflowRule {
            cat,
            op,
            check,
            dest,
        };
    }
    panic!("Could not parse workflow rule.");
}

fn parse_part(pair: Pair<Rule>) -> Part {
    let mut part = Part {
        x: 0,
        m: 0,
        a: 0,
        s: 0,
    };
    let mut current_cat: Option<Category> = None;
    for pair in pair.into_inner().flatten() {
        match pair.as_rule() {
            Rule::category => current_cat = Some(Category::parse(pair.as_str())),
            Rule::number => {
                if let Some(current_cat) = current_cat {
                    let value = pair.as_str().parse::<usize>().unwrap();
                    match current_cat {
                        Category::X => part.x = value,
                        Category::M => part.m = value,
                        Category::A => part.a = value,
                        Category::S => part.s = value,
                    };
                }
            }
            _ => (),
        }
    }
    part
}

struct Part {
    x: usize,
    m: usize,
    a: usize,
    s: usize,
}

impl Part {
    fn category_value(&self, cat: Category) -> usize {
        match cat {
            Category::X => self.x,
            Category::M => self.m,
            Category::A => self.a,
            Category::S => self.s,
        }
    }

    fn rating(&self) -> usize {
        self.x + self.m + self.a + self.s
    }
}

#[derive(Debug)]
struct Workflow {
    name: String,
    rules: Vec<WorkflowRule>,
    default: Option<Destination>,
}

impl Workflow {
    fn apply(&self, part: &Part) -> Destination {
        for rule in &self.rules {
            if let Some(dest) = rule.apply(part) {
                return dest;
            }
        }
        if let Some(default) = &self.default {
            return default.clone();
        }
        panic!("Part did not match any rule in workflow.")
    }
}
#[derive(Debug)]
struct WorkflowRule {
    cat: Category,
    op: WorkflowOp,
    check: usize,
    dest: Destination,
}

impl WorkflowRule {
    fn apply(&self, part: &Part) -> Option<Destination> {
        let value = part.category_value(self.cat);
        if self.op.apply(value, self.check) {
            Some(self.dest.clone())
        } else {
            None
        }
    }
}
#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum WorkflowOp {
    LessThan,
    GreaterThan,
}

impl WorkflowOp {
    fn parse(value: &str) -> Self {
        match value {
            "<" => WorkflowOp::LessThan,
            ">" => WorkflowOp::GreaterThan,
            _ => panic!("Coult not parse workflow op {}", value),
        }
    }
}

#[derive(Debug, PartialEq, Clone, Hash, Eq)]
enum Destination {
    Accept,
    Reject,
    Workflow(String),
}

impl Destination {
    fn parse(value: &str) -> Self {
        match value {
            "A" => Destination::Accept,
            "R" => Destination::Reject,
            _ => Destination::Workflow(value.to_string()),
        }
    }
}

impl WorkflowOp {
    fn apply(&self, left: usize, right: usize) -> bool {
        match &self {
            WorkflowOp::GreaterThan => left > right,
            WorkflowOp::LessThan => left < right,
        }
    }
}
#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum Category {
    X,
    M,
    A,
    S,
}

impl Category {
    fn parse(value: &str) -> Self {
        match value {
            "x" => Category::X,
            "m" => Category::M,
            "a" => Category::A,
            "s" => Category::S,
            _ => panic!("Could not parse category {}", value),
        }
    }
}

impl From<Category> for usize {
    fn from(value: Category) -> Self {
        match value {
            Category::X => 0,
            Category::M => 1,
            Category::A => 2,
            Category::S => 3,
        }
    }
}

#[derive(Parser)]
#[grammar_inline = r#"
WHITESPACE = _{ " " }
number = { '0'..'9'+ }
workflow_name = @{ ASCII_ALPHA+ }
category = { "x" | "m" | "a" | "s" }
comp = { "<" | ">" }
dest = @{ "R" | "A" | workflow_name }
default_dest = @{ dest }
check = { category ~ comp ~ number ~ ":" ~ dest }
workflow = { workflow_name ~ "{" ~ check ~ ("," ~ check)* ~ "," ~ default_dest ~ "}" }

part_cat = { category ~ "=" ~ number }
part = { "{" ~ part_cat ~ ("," ~ part_cat ){3,3} ~ "}" }

file = { SOI ~ workflow ~ (NEWLINE ~ workflow )* ~ NEWLINE ~ (NEWLINE ~ part)* ~ NEWLINE? ~ EOI }
"#]
pub struct WorkflowParser;
