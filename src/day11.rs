use std::cmp::max;
use std::cmp::min;
use std::collections::hash_map::Entry;
use std::collections::HashMap;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() -> Result<(), Box<dyn Error>> {
    let mut grid = parse_file();
    let (exp_rows, exp_cols) = expanded_space(&grid);
    let galaxies: Vec<Point> = find_galaxies(&grid);
    let mut dist_matrix: HashMap<Point, HashMap<Point, usize>> = HashMap::new();
    let mut total_dist: usize = 0;

    for galaxy_a in &galaxies {
        for galaxy_b in &galaxies {
            if galaxy_a == galaxy_b {
                continue;
            }

            let mut k: [Point; 2] = [*galaxy_a, *galaxy_b];
            k.sort();

            let e = dist_matrix
                .entry(k[0])
                .or_insert(HashMap::new())
                .entry(k[1]);

            match e {
                Entry::Occupied(_) => (),
                Entry::Vacant(e) => {
                    let dist = calc_distance(&exp_rows, &exp_cols, &galaxy_a, &galaxy_b);
                    e.insert(dist);
                    total_dist += dist;
                }
            }
        }
    }

    println!("Part One: {total_dist}");

    Ok(())
}

fn calc_distance(
    exp_rows: &Vec<usize>,
    exp_cols: &Vec<usize>,
    galaxy_a: &Point,
    galaxy_b: &Point,
) -> usize {
    let dist = galaxy_a[0].abs_diff(galaxy_b[0]) + galaxy_a[1].abs_diff(galaxy_b[1]);
    let crossed_y = exp_rows
        .iter()
        .filter(|&el| max(galaxy_b[0], galaxy_a[0]) >= *el && *el >= min(galaxy_a[0], galaxy_b[0]))
        .count();
    let crossed_x = exp_cols
        .iter()
        .filter(|&el| max(galaxy_b[1], galaxy_a[1]) >= *el && *el >= min(galaxy_a[1], galaxy_b[1]))
        .count();

    dist + ((crossed_y + crossed_x) * 999999)
}

fn find_galaxies(grid: &Vec<Vec<Space>>) -> Vec<Point> {
    grid.iter()
        .enumerate()
        .flat_map(|(i, row)| {
            row.iter()
                .enumerate()
                .filter(|(_j, &col)| col == Space::Galaxy)
                .map(move |(j, _col)| [i, j])
        })
        .collect()
}

fn expanded_space(grid: &Vec<Vec<Space>>) -> (Vec<usize>, Vec<usize>) {
    let mut insert_rows: Vec<usize> = Vec::new();
    let mut insert_cols: Vec<usize> = Vec::new();
    // There's definitely a way to do this without so many loops
    for (i, row) in grid.iter().enumerate() {
        if row.iter().all(|&el| el == Space::Empty) {
            insert_rows.push(i);
        }
    }

    for (j, _col) in grid[0].iter().enumerate() {
        if (0..grid.len())
            .map(|i| grid[i][j])
            .all(|el| el == Space::Empty)
        {
            insert_cols.push(j);
        }
    }

    (insert_rows, insert_cols)
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum Space {
    Galaxy,
    Empty,
}

type Point = [usize; 2];

fn parse_file() -> Vec<Vec<Space>> {
    let file = File::open("input/day11.txt").unwrap();
    let lines = BufReader::new(file).lines();

    let grid = lines
        .map(|l| l.unwrap())
        .enumerate()
        .map(|(_i, l)| {
            l.chars()
                .enumerate()
                .map(|(_j, ch)| match ch {
                    '.' => Space::Empty,
                    '#' => Space::Galaxy,

                    _ => panic!("Unknown pipe"),
                })
                .collect::<Vec<Space>>()
        })
        .collect::<Vec<Vec<Space>>>();
    grid
}
