use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

#[derive(PartialEq, Debug, Clone, Copy)]
enum Pipe {
    NorthSouth,
    EastWest,
    NorthEast,
    NorthWest,
    SouthWest,
    SouthEast,
    Ground,
    Start,
}

fn main() -> Result<(), Box<dyn Error>> {
    let (start, grid) = parse_file();

    let mut next = neighbours(&grid, start)[0];
    let mut path: Vec<[usize; 2]> = vec![start, next];
    let mut prev = start;

    while grid[next[0]][next[1]] != Pipe::Start {
        next = connected_to(&grid, prev, next);
        prev = *path.iter().last().unwrap();
        path.push(next);
    }

    let steps = path.len() / 2;
    println!("Part One: {steps}");

    // Shoelace algorithm for the area
    let area = (path.windows(2).map(|w| w[0][1] * w[1][0]).sum::<usize>()
        - path.windows(2).map(|w| w[0][0] * w[1][1]).sum::<usize>())
        / 2;

    // Pick's theorem for the interior points
    let b = path.len() - 1;
    let i = area + 1 - b / 2;

    println!("Part Two: {i}");

    Ok(())
}

fn connected_to(grid: &Vec<Vec<Pipe>>, [i, j]: [usize; 2], [y, x]: [usize; 2]) -> [usize; 2] {
    match grid[y][x] {
        Pipe::NorthSouth => {
            if y > i {
                [y + 1, x]
            } else {
                [y - 1, x]
            }
        }
        Pipe::EastWest => {
            if x > j {
                [y, x + 1]
            } else {
                [y, x - 1]
            }
        }
        Pipe::NorthEast => {
            if x < j {
                [y - 1, x]
            } else {
                [y, x + 1]
            }
        }
        Pipe::NorthWest => {
            if x > j {
                [y - 1, x]
            } else {
                [y, x - 1]
            }
        }
        Pipe::SouthWest => {
            if y < i {
                [y, x - 1]
            } else {
                [y + 1, x]
            }
        }
        Pipe::SouthEast => {
            if x < j {
                [y + 1, x]
            } else {
                [y, x + 1]
            }
        }
        Pipe::Ground => panic!("Ran into ground"),
        Pipe::Start => panic!("Loop found"),
    }
}

fn neighbours(grid: &Vec<Vec<Pipe>>, [i, j]: [usize; 2]) -> Vec<[usize; 2]> {
    let mut cells: Vec<[usize; 2]> = Vec::new();

    for dy in 0..=2 {
        for dx in 0..=2 {
            if (dy == 1 && dx == 1)
                || i + dy <= 0
                || j + dx <= 0
                || i + dy - 1 > grid.len() - 1
                || j + dx - 1 > grid[0].len() - 1
            {
                continue;
            }
            if is_connected(grid, [i, j], [i + dy - 1, j + dx - 1]) {
                cells.push([i + dy - 1, j + dx - 1]);
            }
        }
    }
    cells
}

fn is_connected(grid: &Vec<Vec<Pipe>>, [i, j]: [usize; 2], [y, x]: [usize; 2]) -> bool {
    match grid[y][x] {
        Pipe::NorthSouth => x == j,
        Pipe::EastWest => i == y,
        Pipe::NorthEast => (y == i + 1 && j == x) || (y == i && x == j - 1),
        Pipe::NorthWest => (y == i + 1 && j == x) || (y == i && x == j + 1),
        Pipe::SouthWest => (y == i - 1 && j == x) || (y == i && x == j + 1),
        Pipe::SouthEast => (y == i - 1 && j == x) || (y == i && x == j - 1),
        Pipe::Ground => false,
        Pipe::Start => true,
    }
}

fn parse_file() -> ([usize; 2], Vec<Vec<Pipe>>) {
    let file = File::open("input/day10.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut start: [usize; 2] = [0, 0];

    let grid = lines
        .map(|l| l.unwrap())
        .enumerate()
        .map(|(i, l)| {
            l.chars()
                .enumerate()
                .map(|(j, ch)| match ch {
                    '|' => Pipe::NorthSouth,
                    '-' => Pipe::EastWest,
                    'L' => Pipe::NorthEast,
                    'J' => Pipe::NorthWest,
                    '7' => Pipe::SouthWest,
                    'F' => Pipe::SouthEast,
                    '.' => Pipe::Ground,
                    'S' => {
                        start = [i, j];
                        Pipe::Start
                    }
                    _ => panic!("Unknown pipe"),
                })
                .collect::<Vec<Pipe>>()
        })
        .collect::<Vec<Vec<Pipe>>>();
    (start, grid)
}
