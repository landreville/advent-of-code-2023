use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::iter::FromIterator;

use rustworkx_core::connectivity::stoer_wagner_min_cut;
use rustworkx_core::petgraph::graph::UnGraph;
use rustworkx_core::Result;

fn main() -> Result<(), Box<dyn Error>> {
    let components = parse_file();
    let names: HashMap<&str, u32> = HashMap::from_iter(
        components
            .keys()
            .enumerate()
            .map(|(i, el)| (el.as_str(), i as u32)),
    );
    let wires = get_wires(&components);
    // Use indices instead of strings to create graph
    let graph = UnGraph::<(), u32>::from_edges(
        wires
            .iter()
            .map(|(v1, v2)| (*(names.get(v1).unwrap()), *(names.get(v2).unwrap()))),
    );

    let min_cut_res: Result<Option<(usize, Vec<_>)>> = stoer_wagner_min_cut(&graph, |_| Ok(1));
    let (_, partition) = min_cut_res.unwrap().unwrap();

    let total = (names.len() - partition.len()) * partition.len();
    println!("Part One: {total}");

    Ok(())
}

fn get_wires<'a>(components: &'a HashMap<String, HashSet<String>>) -> HashSet<(&'a str, &'a str)> {
    let mut wires = HashSet::new();
    for (key, values) in components {
        for v in values {
            if key < v {
                wires.insert((key.as_str(), v.as_str()));
            } else {
                wires.insert((v.as_str(), key.as_str()));
            }
        }
    }
    wires
}

fn parse_file() -> HashMap<String, HashSet<String>> {
    let file = File::open("input/day25.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut components: HashMap<String, HashSet<String>> = HashMap::new();

    lines.map(|l| l.unwrap()).for_each(|l| {
        let mut parts = l.split(": ");
        let node = parts.next().unwrap();
        let adjacent: Vec<String> = parts
            .next()
            .unwrap()
            .split(" ")
            .map(|p| p.to_string())
            .collect();

        components
            .entry(node.to_string())
            .or_insert(HashSet::new())
            .extend(adjacent.clone());

        for adj in adjacent {
            components
                .entry(adj)
                .or_insert(HashSet::new())
                .insert(node.to_string());
        }
    });

    components
}
