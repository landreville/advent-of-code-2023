use std::cmp::{max, min};
use std::collections::HashSet;
use std::error::Error;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::iter::zip;

fn main() -> Result<(), Box<dyn Error>> {
    let patterns = parse_file();

    let lines: usize = patterns.iter().map(|pattern| find_all_lines(pattern)).sum();

    println!("Part Two: {}", lines);
    Ok(())
}

fn find_all_lines(pattern: &Vec<Vec<Space>>) -> usize {
    let lines_v = find_vertical_mirror(pattern, 0);
    let lines_h = find_horizontal_mirror(pattern, 0);

    let lines_v_new: Option<usize> = find_vertical_with_smudges(pattern, lines_v)
        .into_iter()
        .filter(|&v| v != lines_v)
        .last();
    let lines_h_new: Option<usize> = find_horizontal_with_smudges(pattern, lines_h)
        .into_iter()
        .filter(|&v| v != lines_h)
        .last();

    if let Some(lines) = lines_v_new {
        return lines;
    }
    if let Some(lines) = lines_h_new {
        return lines;
    }
    max(lines_h, lines_v)
}

fn find_horizontal_with_smudges(pattern: &Vec<Vec<Space>>, ignore_value: usize) -> HashSet<usize> {
    let mut line_potentials: HashSet<usize> = HashSet::new();
    for i in 0..pattern.len() {
        for j in 0..pattern.len() {
            if i == j {
                continue;
            }
            if let Some(smudge_index) = is_smudged_at(&pattern[i], &pattern[j]) {
                println!("Found smudge at {i}, {j}, {smudge_index}");
                // Change A
                let mut pattern_a = pattern.clone();
                pattern_a[i] = swap_space(&pattern[i], smudge_index);
                let new_lines_a = find_horizontal_mirror(&pattern_a, ignore_value);
                if new_lines_a != 0 {
                    line_potentials.insert(new_lines_a);
                }

                // Change B
                let mut pattern_b = pattern.clone();
                pattern_b[j] = swap_space(&pattern[j], smudge_index);
                let new_lines_b = find_horizontal_mirror(&pattern_b, ignore_value);
                if new_lines_b != 0 {
                    line_potentials.insert(new_lines_b);
                }
            }
        }
    }
    line_potentials
}

fn find_vertical_with_smudges(pattern: &Vec<Vec<Space>>, ignore_value: usize) -> HashSet<usize> {
    let mut line_potentials: HashSet<usize> = HashSet::new();
    for i in 0..pattern[0].len() {
        for j in 0..pattern[0].len() {
            if i == j {
                continue;
            }
            let col_i: Vec<Space> = pattern.iter().map(|row| row[i]).collect();
            let col_j: Vec<Space> = pattern.iter().map(|row| row[j]).collect();

            if let Some(smudge_index) = is_smudged_at(&col_i, &col_j) {
                // Change A
                let mut pattern_a = pattern.clone();
                let new_col_a = swap_space(&col_i, smudge_index);
                pattern_a
                    .iter_mut()
                    .enumerate()
                    .for_each(|(k, row)| row[i] = new_col_a[k]);
                let new_lines_a = find_vertical_mirror(&pattern_a, ignore_value);
                if new_lines_a != 0 {
                    line_potentials.insert(new_lines_a);
                }

                // Change B
                let mut pattern_b = pattern.clone();
                let new_col_b = swap_space(&col_j, smudge_index);
                pattern_b
                    .iter_mut()
                    .enumerate()
                    .for_each(|(k, row)| row[j] = new_col_b[k]);

                let new_lines_b = find_vertical_mirror(&pattern_b, ignore_value);
                if new_lines_b != 0 {
                    line_potentials.insert(new_lines_b);
                }
            }
        }
    }
    line_potentials
}

fn swap_space(row: &Vec<Space>, smudge_index: usize) -> Vec<Space> {
    let mut new_row = row.clone();
    new_row[smudge_index] = if new_row[smudge_index] == Space::Ash {
        Space::Stone
    } else {
        Space::Ash
    };
    new_row
}

fn is_smudged_at(a: &Vec<Space>, b: &Vec<Space>) -> Option<usize> {
    let mut smudge_index = None;
    let mut edits = 0;

    for (i, (sp_a, sp_b)) in zip(a, b).enumerate() {
        if sp_a != sp_b {
            edits += 1;
            smudge_index = Some(i)
        }
        if edits > 1 {
            return None;
        }
    }

    if edits == 1 {
        smudge_index
    } else {
        None
    }
}

fn find_vertical_mirror(pattern: &Vec<Vec<Space>>, ignore_value: usize) -> usize {
    'lines: for i in 1..pattern[0].len() {
        if pattern
            .iter()
            .map(|row| row[i])
            .eq(pattern.iter().map(|row| row[i - 1]))
        {
            // Mirror is between this and previous
            let max_mirrored = min(i, pattern[0].len() - i);

            for j in 0..max_mirrored {
                if !pattern
                    .iter()
                    .map(|row| row[i - 1 - j])
                    .eq(pattern.iter().map(|row| row[i + j]))
                {
                    continue 'lines;
                }
            }

            if i != ignore_value {
                return i;
            }
        }
    }
    0
}

fn find_horizontal_mirror(pattern: &Vec<Vec<Space>>, ignore_value: usize) -> usize {
    // Don't bother with first and last lines

    'lines: for i in 1..pattern.len() {
        if pattern[i - 1] == pattern[i] {
            // Mirror is between this and previous

            let max_mirrored = min(i, pattern.len() - i);

            for j in 0..max_mirrored {
                if pattern[i - 1 - j] != pattern[i + j] {
                    continue 'lines;
                }
            }

            if i * 100 != ignore_value {
                return i * 100;
            }
        }
    }
    0
}

#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum Space {
    Ash,
    Stone,
}

fn parse_file() -> Vec<Vec<Vec<Space>>> {
    let file = File::open("input/day13.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut patterns = Vec::new();
    let mut current_pattern = Vec::new();

    for line in lines {
        if let Ok(line) = line {
            if line.len() == 0 {
                patterns.push(current_pattern.clone());
                current_pattern = Vec::new();
            } else {
                current_pattern.push(
                    line.chars()
                        .map(|ch| match ch {
                            '.' => Space::Ash,
                            '#' => Space::Stone,
                            _ => panic!("unexpected space"),
                        })
                        .collect::<Vec<Space>>(),
                )
            }
        }
    }
    patterns.push(current_pattern.clone());

    patterns
}
