use num::Integer;
use std::collections::{HashMap, VecDeque};
use std::error::Error;
use std::fmt;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() -> Result<(), Box<dyn Error>> {
    let mut modules = parse_file();
    let pulses = part_two(&mut modules);

    println!("Part Two: {pulses}");
    Ok(())
}

fn part_two(modules: &mut HashMap<String, Mod>) -> usize {
    let mut queue: VecDeque<(String, String, Pulse)> = VecDeque::new();
    let mut counts: [usize; 2] = [0, 0];

    // This is a conjunction module (peek at the input file)
    // and therefore must have all High pulses before it will send a Low to rx.
    let rx_input: String = modules
        .values()
        .find(|&el| el.dests.contains(&"rx".to_string()))
        .map(|el| el.name.clone())
        .unwrap();

    // input module name -> cycle for High pulses
    let mut high_cycles: HashMap<String, usize> = HashMap::from_iter(
        modules
            .values()
            .filter(|&el| el.dests.contains(&rx_input))
            .map(|el| (el.name.clone(), 0)),
    );

    for i in 1.. {
        counts[usize::from(Pulse::Low)] += 1;
        // println!("button -{}-> broadcaster", Pulse::Low);
        queue.push_back((
            "broadcaster".to_string(),
            "broadcaster".to_string(),
            Pulse::Low,
        ));

        while let Some((prev_mod_name, mod_name, pulse)) = queue.pop_front() {
            if let Some(module) = modules.get_mut(&mod_name) {
                match module.mod_type {
                    ModType::Broadcast => {
                        for d in &module.dests {
                            counts[usize::from(pulse)] += 1;
                            // println!("{} -{}-> {}", module.name, pulse, d);
                            if &rx_input == d && pulse == Pulse::High {
                                high_cycles.insert(module.name.clone(), i);
                                if high_cycles.values().all(|v| *v > 0) {
                                    return high_cycles.values().fold(1, |acc, e| e.lcm(&acc));
                                }
                            }
                            queue.push_back((module.name.clone(), d.to_string(), pulse));
                        }
                    }
                    ModType::FlipFlop => {
                        if pulse == Pulse::Low {
                            let next_pulse = if module.state {
                                Pulse::Low
                            } else {
                                Pulse::High
                            };

                            for d in &module.dests {
                                counts[usize::from(next_pulse)] += 1;
                                // println!("{} -{}-> {}", module.name, next_pulse, d);
                                if &rx_input == d && next_pulse == Pulse::High {
                                    high_cycles.insert(module.name.clone(), i);
                                    if high_cycles.values().all(|v| *v > 0) {
                                        return high_cycles.values().fold(1, |acc, e| e.lcm(&acc));
                                    }
                                }
                                queue.push_back((module.name.clone(), d.to_string(), next_pulse));
                            }
                            module.state = !module.state;
                        }
                    }
                    ModType::Conjunction => {
                        module.inputs.insert(prev_mod_name, pulse);

                        let next_pulse = if module.inputs.values().all(|&v| v == Pulse::High) {
                            Pulse::Low
                        } else {
                            Pulse::High
                        };

                        for d in &module.dests {
                            counts[usize::from(next_pulse)] += 1;
                            // println!("{} -{}-> {}", module.name, next_pulse, d);
                            if &rx_input == d && next_pulse == Pulse::High {
                                high_cycles.insert(module.name.clone(), i);
                                if high_cycles.values().all(|v| *v > 0) {
                                    return high_cycles.values().fold(1, |acc, e| e.lcm(&acc));
                                }
                            }
                            queue.push_back((module.name.clone(), d.to_string(), next_pulse));
                        }
                    }
                }
            }
        }
        // println!("Low: {}, High: {} \n", counts[0], counts[1]);
    }
    // counts.iter().product()
    panic!("Not found")
}

#[derive(Debug, PartialEq, Clone, Eq)]
struct Mod {
    mod_type: ModType,
    name: String,
    dests: Vec<String>,
    inputs: HashMap<String, Pulse>,
    state: bool,
}
#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum Pulse {
    High,
    Low,
}
impl fmt::Display for Pulse {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Pulse::High => write!(f, "high"),
            Pulse::Low => write!(f, "low"),
        }
    }
}

impl From<Pulse> for usize {
    fn from(value: Pulse) -> Self {
        match value {
            Pulse::Low => 0,
            Pulse::High => 1,
        }
    }
}

#[derive(Debug, PartialEq, Clone, Copy, Hash, Eq)]
enum ModType {
    FlipFlop,
    Conjunction,
    Broadcast,
}

fn parse_file() -> HashMap<String, Mod> {
    let file = File::open("input/day20.txt").unwrap();
    let lines = BufReader::new(file).lines();
    // Destination -> Inputs
    let mut inputs: HashMap<String, Vec<String>> = HashMap::new();

    let mut modules = HashMap::from_iter(lines.map(|l| l.unwrap()).map(|l| {
        let mod_type = match &l[0..1] {
            "%" => ModType::FlipFlop,
            "&" => ModType::Conjunction,
            "b" => ModType::Broadcast,
            _ => panic!("Unexpected module type {:?}", &l[0..1]),
        };
        let mut parts = l[1..].split(" -> ");
        let name = if mod_type == ModType::Broadcast {
            parts.next();
            "broadcaster"
        } else {
            parts.next().unwrap()
        };
        let dests: Vec<String> = parts
            .next()
            .unwrap()
            .split(", ")
            .map(|s| s.to_string())
            .collect();

        for dest in &dests {
            inputs
                .entry(dest.clone())
                .or_insert(Vec::new())
                .push(name.to_string())
        }
        (
            name.to_string(),
            Mod {
                mod_type,
                name: name.to_string(),
                dests: dests.clone(),
                inputs: HashMap::new(),
                state: mod_type != ModType::FlipFlop,
            },
        )
    }));

    for (dest, input_list) in inputs {
        modules.entry(dest).and_modify(|m| {
            input_list.iter().for_each(|i| {
                m.inputs.insert(i.to_string(), Pulse::Low);
            })
        });
    }

    modules
}
