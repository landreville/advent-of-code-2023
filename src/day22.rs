use regex::Regex;
use std::cmp::{max, min, Ordering};
use std::collections::{BinaryHeap, HashMap, HashSet};
use std::error::Error;
use std::fmt;
use std::fmt::Formatter;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;

fn main() -> Result<(), Box<dyn Error>> {
    let mut bricks = parse_file();
    part_one_and_two(&mut bricks);
    Ok(())
}

fn part_one_and_two(bricks: &mut BinaryHeap<Brick>) {
    let floor = bricks.iter().map(|b| b.min_z()).min().unwrap();
    let mut fallen = Vec::new();
    let mut supported_by = HashMap::new();
    let mut supporting = HashMap::new();

    while let Some(mut brick) = bricks.pop() {
        if brick.min_z() == floor {
            fallen.push(brick);
            continue;
        }
        let mut support_level = 0;
        let mut support = Vec::new();
        for i in 0..fallen.len() {
            let fallen_brick = fallen[i];
            if fallen_brick.start.x <= brick.end.x
                && brick.start.x <= fallen_brick.end.x
                && fallen_brick.start.y <= brick.end.y
                && brick.start.y <= fallen_brick.end.y
            {
                let top = fallen_brick.max_z();
                if top > support_level {
                    support_level = top;
                    support.clear();
                }

                if top == support_level {
                    support.push(fallen_brick);
                }
            }
        }
        brick.move_to_z(support_level + 1);
        supported_by
            .entry(brick)
            .or_insert(Vec::new())
            .extend(support.clone());

        for s in support {
            supporting.entry(s).or_insert(Vec::new()).push(brick);
        }

        fallen.push(brick);
    }

    let mut total = 0;
    for i in 0..fallen.len() {
        let brick = fallen[i];
        if let Some(bricks_above) = supporting.get(&brick) {
            if bricks_above
                .iter()
                .all(|ba| supported_by.get(&ba).unwrap().len() > 1)
            {
                total += 1;
            }
        } else {
            total += 1;
        }
    }
    println!("Part One: {total}");
    let mut total = 0;
    for i in 0..fallen.len() {
        let brick = fallen[i];
        let mut above = BinaryHeap::new();
        supporting.get(&brick).is_some_and(|bricks_above| {
            bricks_above
                .iter()
                .filter(|&b| supported_by.get(b).unwrap().len() == 1)
                .for_each(|b| above.push(b));
            true
        });
        let mut broken = HashSet::new();

        while let Some(brick_above) = above.pop() {
            broken.insert(brick_above);

            if let Some(supporting_bricks) = supporting.get(&brick_above) {
                for s in supporting_bricks {
                    if (HashSet::from_iter(supported_by.get(s).unwrap()).difference(&broken))
                        .count()
                        == 0
                    {
                        above.push(s);
                    }
                }
            }
        }
        total += broken.len();
    }
    println!("Part Two: {total}");
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
struct Point {
    x: isize,
    y: isize,
    z: isize,
}

impl fmt::Display for Point {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "[{}, {}, {}]", self.x, self.y, self.z)
    }
}

impl fmt::Debug for Point {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self)
    }
}

#[derive(Debug, Clone, Copy, Eq, Hash)]
struct Brick {
    start: Point,
    end: Point,
}

impl PartialEq<Self> for Brick {
    fn eq(&self, other: &Self) -> bool {
        self.start == other.start && self.end == other.end
    }
}

impl PartialOrd<Self> for Brick {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(&other))
    }
}

impl Ord for Brick {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        other.min_z().cmp(&self.min_z())
    }
}

impl Brick {
    fn min_z(&self) -> isize {
        min(self.start.z, self.end.z)
    }

    fn max_z(&self) -> isize {
        max(self.start.z, self.end.z)
    }
    fn move_to_z(&mut self, new_z: isize) {
        if self.start.z < self.end.z {
            self.end.z = self.end.z - (self.start.z - new_z);
            self.start.z = new_z;
        } else {
            self.start.z = self.start.z - (self.end.z - new_z);
            self.end.z = new_z;
        }
    }
}

fn parse_file() -> BinaryHeap<Brick> {
    let file = File::open("input/day22.txt").unwrap();
    let lines = BufReader::new(file).lines();
    let mut bricks = BinaryHeap::new();
    let re =
        Regex::new(r"(?<x1>\d+),(?<y1>\d+),(?<z1>\d+)~(?<x2>\d+),(?<y2>\d+),(?<z2>\d+)").unwrap();

    lines.map(|l| l.unwrap()).for_each(|l| {
        let Some(caps) = re.captures(&l) else {
            panic!("Invalid brick. {}", l);
        };

        bricks.push(Brick {
            start: Point {
                x: caps["x1"].parse::<isize>().unwrap(),
                y: caps["y1"].parse::<isize>().unwrap(),
                z: caps["z1"].parse::<isize>().unwrap(),
            },
            end: Point {
                x: caps["x2"].parse::<isize>().unwrap(),
                y: caps["y2"].parse::<isize>().unwrap(),
                z: caps["z2"].parse::<isize>().unwrap(),
            },
        });
    });

    bricks
}
